
module mod(out, clk, reset);
    parameter   WIDTH = 8;
    output      [WIDTH - 1: 0]      out;
    input                           clk, reset;

    reg [WIDTH - 1 : 0]             out;
    wire                            clk, reset;
    
    always@(posedge clk)
    begin
        // do stuff ..
    end
endmodule
