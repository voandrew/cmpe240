`timescale 100ns/10ns
module test;
/* Make a reset that pulses once. */
reg clk = 1;
reg in = 0;
wire out;

initial begin
    $dumpfile("test.vcd");
    $dumpvars(0,test);

    # 17 clk = 1;
    # 11 clk = 0;
    # 29 clk = 1;
    # 31 clk = 0;
    # 33 clk = 1;
    # 38 clk = 0;
    # 55 clk = 1;
    # 513 $finish;
end

/* Make a regular pulsing clock. */
always #1 in = !in;

latch l1(.out(out), 
       .in(in), 
       .clk(clk));

initial
 $monitor("At time %t, in = %b out = %b",
          $time, in, out);
endmodule // test
