
module latch(out, in, clk);

input   in, clk;
output  reg out;
initial begin
    out = 0;
end

always @(in or clk)
begin
    if(~clk)
    begin
        out <= in;
    end
end

endmodule
