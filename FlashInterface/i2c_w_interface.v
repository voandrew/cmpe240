module i2c_w_interface (
    input wire sclMaster, sclAux, sda,
    inout wire [7:0] dataIo,
    output wire [15:0] addrOut,
    output wire nEn, nWe
);

parameter ADDR_WIDTH = 16;
parameter DATA_WIDTH = 8;

// outputs
wire i2cStart, i2cStop;
wire   [ADDR_WIDTH-1:0] addrRegOut;
wire   [DATA_WIDTH-1:0] shiftRegOut;
// inout  [DATA_WIDTH-1:0] dataIo; // only view output 

// control signals
wire shiftEnable;
wire loadAddrLSB, loadAddrMSB, incrAddr;
wire selAddr, sel5555, selAAAA;
wire selData, selAA, sel55, selB0, selC0, selD0, selE0, enDataOut;
wire scl, selAux, enSdaOut;

// aux scl selector
assign scl = (selAux ? sclAux : sclMaster);

nmos sdaPull(sda, 1'b0, enSdaOut);

i2c_w_controller i2cWriteController(
    .scl(scl),
    .shiftEnable(shiftEnable),
    .loadAddrLSB(loadAddrLSB),
    .loadAddrMSB(loadAddrMSB),
    .loadDevId  (loadDevId),
    .incrAddr   (incrAddr),
    .selAddr    (selAddr),
    .sel5555    (sel5555),
    .selAAAA    (selAAAA),
    .selData    (selData),
    .selAA      (selAA),
    .sel55      (sel55),
    .selB0      (selB0),
    .selC0      (selC0),
    .selD0      (selD0),
    .selE0      (selE0),
    .enDataOut  (enDataOut),
    .nEn        (nEn),
    .nWe        (nWe),
    .enSdaOut   (enSdaOut),
    .selAux     (selAux),
    .start      (i2cStart),
    .stop       (i2cStop),
    .writeBit   (shiftRegOut[7])
    );

i2c_start_stop_detector i2cDetector(
    .scl  (scl),
    .sda  (sda),
    .start(i2cStart),
    .stop (i2cStop)
    );

shift_reg shiftReg1(
    .shiftRegOut(shiftRegOut),
    .shiftIn    (shiftEnable),
    .SDAdataIn  (sda),
    .SCL        (scl),
    .loadShift  (1'b0),
    .shiftRegIn (8'h00),
    .shiftOut   (1'b0)
    // .SDAdataOut (1'bz),
    // .shiftOut   (1'b0)
    );

i2c_w_addr_counter i2cAddrCounter(
    .addrOut    (addrRegOut),
    .shiftIn    (shiftRegOut),
    .loadAddrLSB(loadAddrLSB),
    .loadAddrMSB(loadAddrMSB),
    .incrAddr   (incrAddr),
    .scl        (scl)
    );

i2c_w_data_mux i2cWriteDataMux(
    .dataIo   (dataIo),
    .dataInput(shiftRegOut),
    .selData  (selData),
    .selAA    (selAA),
    .sel55    (sel55),
    .selB0    (selB0),
    .selC0    (selC0),
    .selD0    (selD0),
    .selE0    (selE0),
    .enDataOut(enDataOut),
    .scl      (scl)
    );

i2c_w_addr_mux i2cAddrMux(
    .addrOut(addrOut),
    .addrIn (addrRegOut),
    .selAddr(selAddr),
    .sel5555(sel5555),
    .selAAAA(selAAAA)
    );
endmodule // i2c_w_data_mux