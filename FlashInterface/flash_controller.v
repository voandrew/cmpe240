module flash_controller(nMemEnable, nMemRead, nMemWrite, nMemReset, address, data, nEnable, nRead, nWrite, nReset, decoderEnable);

parameter ADDR_WIDTH    = 16;
parameter DATA_WIDTH    = 8;

reg [5:0] state;
parameter S_IDLE                = 4'b0000; // idle. ready for a command
// preamble states
parameter S_CMD_PRE1            = 4'b0001; // currently processing a command
parameter S_CMD_PRE2            = 4'b0010; // currently processing a command
parameter S_CMD_CODE            = 4'b0011; // getting code
// command states
parameter S_READ                = 4'b0100;
parameter S_WRITE               = 4'b0101;
parameter S_FAST_WRITE          = 4'b0110;
parameter S_FAST_WRITE_WAIT     = 4'b0111;
parameter S_FAST_WRITE_NOW      = 4'b1000;
parameter S_FAST_WRITE_RST_WAIT = 4'b1001;
parameter S_RESET               = 4'b1010;

parameter CODE_READ             = 8'h00;
parameter CODE_WRITE            = 8'h20;
parameter CODE_FASTWRITE_SET    = 8'hB0;
parameter CODE_FASTWRITE_CONT   = 8'hC0;
parameter CODE_FASTWRITE_RST1   = 8'hD0;
parameter CODE_FASTWRITE_RST2   = 8'hE0;
parameter CODE_RESET            = 8'hF0;

parameter PREAM_ADDR5           = 16'h5555;
parameter PREAM_ADDRA           = 16'hAAAA;
parameter PREAM_DATA5           = 8'h55;
parameter PREAM_DATAA           = 8'hAA;

input wire nEnable, nRead, nWrite, nReset;
input wire [ADDR_WIDTH-1: 0] address;
input wire [DATA_WIDTH-1: 0] data;
output reg nMemEnable, nMemRead, nMemWrite, nMemReset, decoderEnable;

initial
begin
state = S_IDLE;
nMemEnable <= 1;  nMemWrite <= 1;   nMemRead <= 1;   nMemReset <= 1;
decoderEnable <= 1;
end

always@(posedge nEnable or posedge nWrite or posedge nRead or posedge nReset)
begin
    // $display("\nPOSEDGE ADDR: %h  DATA %h", address, data);
    // $display("enable: %b, write %b, read %b, reset %b", nEnable, nWrite, nRead, nReset);
nMemEnable  <= nEnable;
nMemWrite   <= nWrite;
nMemRead    <= nRead;
nMemReset   <= nReset;
end

// always@(negedge nEnable or negedge nWrite or negedge nRead or negedge nReset)
always@(negedge nEnable)
begin
/* check if in command mode
    * if not in command mode, sit in same state
    */
    if(nEnable == 0 && nWrite == 0 && nRead == 1 && nReset == 1)
    begin
        if(state == S_IDLE)
        begin
            // $display("CHANGING STATE");
            nMemEnable <= 1;  nMemWrite <= 1;   nMemRead <= 1;   nMemReset <= 1;
            decoderEnable <= 1;
            state = S_CMD_PRE1;
        end
    end

    case(state)
        S_IDLE:
        begin
            state <= S_IDLE;
        end
        S_CMD_PRE1:
        begin
            if(address == PREAM_ADDR5 && data == PREAM_DATAA)
            begin
                $display("CMD FOUND");
                state <= S_CMD_PRE2;  // look for preamble 2 next
            end
            else
            begin
                $display("Waiting for command: ADDR %h, DATA %h", address, data);
                state <= S_IDLE;
            end
        end
        S_CMD_PRE2:
        begin
            if(address == PREAM_ADDRA && data == PREAM_DATA5)
            begin
                state <= S_CMD_CODE;  // look for code next
            end
            else
            begin
                state <= S_CMD_PRE2;
            end
        end
        S_CMD_CODE:
        begin
            if (address == PREAM_ADDR5)
            begin
                case(data)
                    CODE_READ:
                    begin
                        state <= S_READ;
                    end
                    CODE_WRITE:
                    begin
                        state <= S_WRITE;
                    end
                    CODE_FASTWRITE_SET:
                    begin
                        state <= S_FAST_WRITE;
                    end
                    CODE_RESET:
                    begin
                        state <= S_RESET;
                    end
                    default:
                    begin
                        state <= S_IDLE;
                    end
                endcase
            end
            else
            begin
                $display("BAD PREAMBLE");
                state <= S_IDLE;
            end
        end
        S_READ:
        begin
            if(!nEnable && nWrite && !nRead && nReset)
            begin
                nMemEnable <= 0;  nMemWrite <= 1;   nMemRead <= 0;   nMemReset <= 1;
                decoderEnable <= 1;
                $display("READ FROM %h", address);
                state <= S_IDLE;
            end
            else
            begin
                // $display("DELAY");
                // $display("FROM %h", address);
                // $display("enable: %b, write %b, read %b, reset %b", nEnable, nWrite, nRead, nReset);
            end
        end
        S_WRITE:
        begin
            if(!nEnable && !nWrite && nRead && nReset)
            begin
                nMemEnable <= 0;  nMemWrite <= 0;   nMemRead <= 1;   nMemReset <= 1;
                decoderEnable <= 1;
                $display("WRITE %h TO %h", data, address);
                state <= S_IDLE;
            end
        end
        S_FAST_WRITE:
        begin
            nMemEnable <= 1;  nMemWrite <= 1;   nMemRead <= 1;   nMemReset <= 1;
            decoderEnable <= 0;
            if(data == CODE_FASTWRITE_CONT)
            begin
                state = S_FAST_WRITE_NOW;
            end
            else if (data == CODE_FASTWRITE_RST1)
            begin
                state <= S_FAST_WRITE_RST_WAIT;
                $display("Wait for next reset code");
            end
        end
        S_FAST_WRITE_WAIT:
        begin
            if(data == CODE_FASTWRITE_CONT)
            begin
                $display("FAST WRITE CODE FOUND");
                nMemEnable <= 1;  nMemWrite <= 1;   nMemRead <= 1;   nMemReset <= 1;
                decoderEnable <= 0;
                state = S_FAST_WRITE_NOW;
            end
            else
            begin
                state <= S_IDLE;
                $display("Cancelled..? DATA: %h", data);
            end
        end
        S_FAST_WRITE_NOW:
        begin
            $display("FAST WRITE %h TO %h", data, address);
            if(!nEnable && !nWrite && nRead && nReset)
            begin
                nMemEnable <= 0;  nMemWrite <= 0;   nMemRead <= 1;   nMemReset <= 1;
                decoderEnable <= 1;
                state <= S_FAST_WRITE;
            end
        end
        S_FAST_WRITE_RST_WAIT:
        begin
            if(data == CODE_FASTWRITE_RST2)
            begin
                nMemEnable <= 1;  nMemWrite <= 1;   nMemRead <= 1;   nMemReset <= 1;
                decoderEnable <= 0;
                $display("FAST WRITE RESET");
                state <= S_IDLE;
            end
        end
        S_RESET:
        begin
            nMemEnable <= 1;  nMemWrite <= 1;   nMemRead <= 1;   nMemReset <= 1;
            decoderEnable <= 0;
            $display("RESET");
        end
    endcase

end
endmodule
