module i2c_start_stop_detector (
    input scl, 
    input sda, 
    output reg start,
    output reg stop
);

initial begin  
    start <= 0;
    stop <= 0;
end

always @(negedge sda or negedge scl)
begin
    if(scl === 1 && sda === 0)
    begin
        start <= 1;
    end
    else begin
        start <= 0;
    end
end
always @(posedge sda or negedge scl)
begin
    if(scl === 1 && sda === 1)
    begin
        stop <= 1;
    end
    else begin
        stop <= 0;
    end
end

endmodule