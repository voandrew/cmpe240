`timescale 100ns/10ns

module test_flash_controller;

/* instantiate variables */
reg clk;
parameter ADDR_WIDTH = 16;
parameter DATA_WIDTH = 8;
reg     [ADDR_WIDTH - 1:0] address;
reg     [DATA_WIDTH - 1:0] data;
reg     nEnable, nRead, nWrite, nReset;
wire    decoderEnable;
reg     decoderEnable_ex;

wire [3:0] EWRS ;    // Enable Write Read Reset
flash_controller fc1(
    .address(address),
    .data(data),
    .nEnable(nEnable),
    .nRead(nRead),
    .nWrite(nWrite),
    .nReset(nReset),
    .decoderEnable(decoderEnable),
    .nMemEnable(EWRS[3]),
    .nMemWrite(EWRS[2]),
    .nMemRead(EWRS[1]),
    .nMemReset(EWRS[0])
);


wire [3:0] bankselect;
wire [3:0] blockselect;
wire [7:0] rowselect;

addr_decoder addr_d_1(
    .address(address),
    .bankselect(bankselect),
    .blockselect(blockselect),
    .rowselect(rowselect)
    );

generate
    genvar i;
    for(i = 0; i < 16; i = i + 1)
    begin
        mem_bank bank(
            .block_no(blockselect),
            .addr_block(rowselect),
            .enable(EWRS[3]),
            .write_en(EWRS[2]),
            .read_en(EWRS[1]),
            .reset(EWRS[0])
            );
    end
endgenerate

/* Make a regular pulsing clock. */
parameter CLOCK_PERIOD = 4;
// always #2 clk = !clk;

initial begin
    $dumpfile("flash_controller.vcd"); // filename of wavefile
    $dumpvars(0,fc1);    // dump instance m1
end

/* setup console monitor format */
// initial begin
//     $display("\ttime, nEnable, nRead, nWrite, nReset, decoderEnable, modeCode, modeCode_ex");
// end

/* setup test condition */
// always @ (*)
// begin
// if(EWRS != EWRS_ex)
//     $display("Unexpected Enable/Write/Read/Reset");
//     $display("EXP: nEN: %b, nWR: %b, nRE: %b, nRST: %b", EWRS_ex[3], EWRS_ex[2], EWRS_ex[1], EWRS_ex[0]);
//     $display("GOT: nEN: %b, nWR: %b, nRE: %b, nRST: %b", EWRS[3], EWRS[2], EWRS[1], EWRS[0]);
// end

/* initial conditions */
initial begin
    clk                 = 0;
    address             = 0;
    nEnable             = 1;
    nRead               = 1;
    nWrite              = 1;
    nReset              = 1;
end

always@ (*)
begin
    if(nEnable && nWrite && nRead && nReset)
    begin
        data <= {DATA_WIDTH{1'bz}};
    end
    else data <= data;
end

/* runtime conditions */
initial
begin
    $display("address, data, nEnable, nWrite, nRead, nReset, nMemEnable, nMemWrite, nMemRead, nMemReset, nMemEnableExpect, nMemWriteExpect, nMemReadExpect, nMemResetExpect");
    // test GOOD read command
    $display("testing GOOD read command...");
    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h5555;
    data    <= 8'hAA;
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;
    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
        address, data,
        nEnable, nWrite, nRead, nReset,
        EWRS[3], EWRS[2], EWRS[1], EWRS[0],
        1'b1, 1'b1, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'hAAAA;
    data    <= 8'h55;
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;
    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
        address, data,
        nEnable, nWrite, nRead, nReset,
        EWRS[3], EWRS[2], EWRS[1], EWRS[0],
        1'b1, 1'b1, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h5555;
    data    <= 8'h00;
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;
    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
        address, data,
        nEnable, nWrite, nRead, nReset,
        EWRS[3], EWRS[2], EWRS[1], EWRS[0],
        1'b1, 1'b1, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 1; nRead <= 0; nReset <= 1;
    address <= 16'h1234;
    // data    <= 2'hFF;    // <- data comes here

    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
        address, data,
        nEnable, nWrite, nRead, nReset,
        EWRS[3], EWRS[2], EWRS[1], EWRS[0],
        1'b0, 1'b1, 1'b0, 1'b1);

    $display("Done testing a GOOD read command");
    // done testing read command
    #CLOCK_PERIOD
    nEnable <= 1;   // put in hibernate mode

    // test BAD read command
    $display("Testing a bad read command...");
    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h5555;
    data    <= 8'hAA;
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b1, 1'b1, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'hAAAA;
    data    <= 8'h55;
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b1, 1'b1, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h5552;    // IMPROPER PREAMBLE
    data    <= 8'h00;
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b1, 1'b1, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 1; nRead <= 0; nReset <= 1;
    address <= 16'h1131;
    // data    <= 2'hFF;    // <- data comes here
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b1, 1'b1, 1'b1, 1'b1);
    $display("Done testing bad read command");
    // done testing BAD read command

    // test GOOD write command
    $display("Testing write command...");
    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h5555;
    data    <= 8'hAA;
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b1, 1'b1, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'hAAAA;
    data    <= 8'h55;
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;
    
    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b1, 1'b1, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h5555;
    data    <= 8'h20;       // write command byte
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;
    
    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b1, 1'b1, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h4431;
    data    <= 8'hFF;    // <- data comes here
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;
    
    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b0, 1'b0, 1'b1, 1'b1);
    $display("Done testing write command");

    // test GOOD fast write command
    $display("Testing fast write command");
    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h5555;
    data    <= 8'hAA;
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b1, 1'b1, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'hAAAA;
    data    <= 8'h55;
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b1, 1'b1, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h5555;
    data    <= 8'hB0;       // fast write set command byte
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b1, 1'b1, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h4431;
    data    <= 8'hC0;       // fast write continue (write)
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b1, 1'b1, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h4431;
    data    <= 8'h88;       // fast write actual data write
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b0, 1'b0, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h4436;
    data    <= 8'hC0;       // fast write continue (write)
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b1, 1'b1, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h4436;
    data    <= 8'h9F;       // fast write actual data write
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b0, 1'b0, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h4436;
    data    <= 8'hC0;       // fast write continue (write)
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b1, 1'b1, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h4440;
    data    <= 8'h76;       // fast write actual data write
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b0, 1'b0, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h4436;
    data    <= 8'hD0;       // fast write RESET1
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b1, 1'b1, 1'b1, 1'b1);

    #CLOCK_PERIOD
    nEnable <= 0;   nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h4440;
    data    <= 8'hE0;       // fast write RESET2
    #CLOCK_PERIOD
    nEnable <= 1;   nWrite <= 1; nRead <= 1; nReset <= 1;
    #4 $finish;

    $display("%h, %h, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b, %b",
    address, data,
    nEnable, nWrite, nRead, nReset,
    EWRS[3], EWRS[2], EWRS[1], EWRS[0],
    1'b1, 1'b1, 1'b1, 1'b1);
    $display("Done testing fast write command");
end
endmodule
