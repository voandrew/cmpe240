`timescale 100ns/10ns

module shift_reg(shiftRegOut, SDAdataOut, shiftRegIn, SDAdataIn, shiftIn, shiftOut, loadShift, SCL);

    input SDAdataIn, shiftIn, shiftOut, loadShift, SCL;
    input [7:0] shiftRegIn;
    output [7:0] shiftRegOut;
    output SDAdataOut;
    reg [7:0] shiftReg = 0;

    assign shiftRegOut = shiftReg;
    assign SDAdataOut = shiftReg[0];

    always@(negedge SCL)
    begin

        if (shiftIn && !shiftOut)
        begin
            #0.5
            shiftReg <= shiftReg >> 1;
            shiftReg[7] <= SDAdataIn;
            // $display("concat = %h", {SDAdataIn,shiftReg[6:0]});
        end

        if (!shiftIn && shiftOut)
        begin
            shiftReg = shiftReg >> 1;
        end

        if (loadShift)
        begin
            #0.5
            shiftReg = shiftRegIn;
        end

    end

endmodule
