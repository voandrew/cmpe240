module mem_bank(data_in_out, block_no, addr_block, enable, read_en, write_en, reset);

parameter BLOCK_WIDTH   = 4;
parameter ADDR_WIDTH    = 8;
parameter DATA_WIDTH    = 8;

inout [DATA_WIDTH-1:0] data_in_out;
output reg [DATA_WIDTH-1:0] data_out;

input [ADDR_WIDTH-1:0] addr_block;
input [BLOCK_WIDTH-1:0] block_no;
input enable, read_en, write_en, reset;

reg [(2**BLOCK_WIDTH)-1:0] block_sel;

integer j;

always@*
begin
    for(j = 0; j < 2**BLOCK_WIDTH; j = j + 1)
    begin
        if (block_no == j)
            block_sel[j] <= 1'b0;
        else
            block_sel[j] <= 1'b1;
    end


end

generate
    genvar i;
    for (i = 0; i < 2**BLOCK_WIDTH; i=i+1)
    begin

                mem_block block (
                    .data_in_out(data_in_out),
                    .addr(addr_block),
                    .enable(block_sel[i]),
                    .read_en(read_en),
                    .write_en(write_en),
                    .reset(reset));

    end
endgenerate

endmodule
