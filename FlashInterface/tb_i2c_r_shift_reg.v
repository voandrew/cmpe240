`timescale 100ns / 100ns

module test_shift_reg;

/* instantiate variables */
reg [7:0] shiftRegIn;
reg SCL, shiftIn, shiftOut, loadShift;
wire [7:0] shiftRegOut;
wire SDAdataOut;
reg SDAdataIn;


/* initial conditions */
initial begin
    SCL = 1;
    SDAdataIn = 0;
end

/* Make a regular pulsing clock. */
always #5 SCL = !SCL;

initial begin
    $dumpfile("shift_reg.vcd"); // filename of wavefile
    $dumpvars(0,sr1);    // dump instance c1
end

/* instantiate module */

shift_reg sr1(  .shiftRegOut(shiftRegOut),
                .SDAdataOut(SDAdataOut),
                .shiftRegIn(shiftRegIn),
                .SDAdataIn(SDAdataIn),
                .shiftIn(shiftIn),
                .shiftOut(shiftOut),
                .loadShift(loadShift),
                .SCL(SCL));


                    /* setup console monitor format */

initial begin
    $monitor("Time: %t | shiftIn = %b | shiftOut = %b | loadShift = %b | SCL = %b\nSDAdataOut = %b, SDAdataIn = %b | shiftRegOut = %b (%h) | shiftRegIn = %b (%h)\n\n ------------------------------------------------------\n",
    $time, shiftIn, shiftOut, loadShift, SCL, SDAdataOut, SDAdataIn, shiftRegOut, shiftRegOut, shiftRegIn, shiftRegIn);
end

initial begin

    #0  shiftRegIn = 8'hAA; loadShift = 0;  shiftIn = 1; shiftOut = 0;
    // Should load shift register
    #5  shiftRegIn = 8'hFB; loadShift = 1;  shiftIn = 0; shiftOut = 0;
    // should feed "FB" out of register over SDAdataOut bit by bit
    // starting with least significant bit
    #5  shiftRegIn = 8'hFF; loadShift = 0;  shiftIn = 0; shiftOut = 1;
        SDAdataIn  = 0;
    #5  shiftRegIn = 8'hFF; loadShift = 0;  shiftIn = 0; shiftOut = 1;
        SDAdataIn  = 0;
    #5  shiftRegIn = 8'hFF; loadShift = 0;  shiftIn = 0; shiftOut = 1;
        SDAdataIn  = 0;
    #5  shiftRegIn = 8'hFF; loadShift = 0;  shiftIn = 0; shiftOut = 1;
        SDAdataIn  = 1;
    #5  shiftRegIn = 8'hFF; loadShift = 0;  shiftIn = 0; shiftOut = 1;
        SDAdataIn  = 1;
    #5  shiftRegIn = 8'hFF; loadShift = 0;  shiftIn = 0; shiftOut = 1;
        SDAdataIn  = 1;
    #5  shiftRegIn = 8'hFF; loadShift = 0;  shiftIn = 0; shiftOut = 1;
        SDAdataIn  = 1;
    #5  shiftRegIn = 8'hFF; loadShift = 0;  shiftIn = 0; shiftOut = 1;
        SDAdataIn  = 1;
    #5  shiftRegIn = 8'hFF; loadShift = 0;  shiftIn = 0; shiftOut = 1;
        SDAdataIn  = 0;
    #5  shiftRegIn = 8'hFF; loadShift = 0;  shiftIn = 0; shiftOut = 1;
        SDAdataIn  = 0;
    #5  shiftRegIn = 8'hFF; loadShift = 0;  shiftIn = 0; shiftOut = 1;
        SDAdataIn  = 0;
    #5  shiftRegIn = 8'hFF; loadShift = 0;  shiftIn = 0; shiftOut = 1;
        SDAdataIn  = 1;
    #5  shiftRegIn = 8'hFF; loadShift = 0;  shiftIn = 0; shiftOut = 1;
        SDAdataIn  = 1;
    #5  shiftRegIn = 8'hFF; loadShift = 0;  shiftIn = 0; shiftOut = 1;
        SDAdataIn  = 1;
    #5  shiftRegIn = 8'hFF; loadShift = 0;  shiftIn = 0; shiftOut = 1;
        SDAdataIn  = 1;
    #5  shiftRegIn = 8'hFF; loadShift = 0;  shiftIn = 0; shiftOut = 1;


    #5  shiftRegIn = 8'hDA; loadShift = 1;  shiftIn = 0; shiftOut = 0;
        SDAdataIn  = 0;
    #5  shiftRegIn = 8'hDA; loadShift = 1;  shiftIn = 0; shiftOut = 0;
        SDAdataIn  = 0;

    // Load AA into reg from SDAdataIn
    #5  shiftRegIn = 8'hDD; loadShift = 0;  shiftIn = 1; shiftOut = 0;
        SDAdataIn  = 0;
    #5  shiftRegIn = 8'hDD; loadShift = 0;  shiftIn = 1; shiftOut = 0;
        SDAdataIn  = 0;
    #5  shiftRegIn = 8'hDD; loadShift = 0;  shiftIn = 1; shiftOut = 0;
        SDAdataIn  = 1;
    #5  shiftRegIn = 8'hDD; loadShift = 0;  shiftIn = 1; shiftOut = 0;
        SDAdataIn  = 1;
    #5  shiftRegIn = 8'hDD; loadShift = 0;  shiftIn = 1; shiftOut = 0;
        SDAdataIn  = 0;
    #5  shiftRegIn = 8'hDD; loadShift = 0;  shiftIn = 1; shiftOut = 0;
        SDAdataIn  = 0;
    #5  shiftRegIn = 8'hDD; loadShift = 0;  shiftIn = 1; shiftOut = 0;
        SDAdataIn  = 1;
    #5  shiftRegIn = 8'hDD; loadShift = 0;  shiftIn = 1; shiftOut = 0;
        SDAdataIn  = 1;
    #5  shiftRegIn = 8'hDD; loadShift = 0;  shiftIn = 1; shiftOut = 0;
        SDAdataIn  = 0;
    #5  shiftRegIn = 8'hDD; loadShift = 0;  shiftIn = 1; shiftOut = 0;
        SDAdataIn  = 0;
    #5  shiftRegIn = 8'hDD; loadShift = 0;  shiftIn = 1; shiftOut = 0;
        SDAdataIn  = 1;
    #5  shiftRegIn = 8'hDD; loadShift = 0;  shiftIn = 1; shiftOut = 0;
        SDAdataIn  = 1;
    #5  shiftRegIn = 8'hDD; loadShift = 0;  shiftIn = 1; shiftOut = 0;
        SDAdataIn  = 0;
    #5  shiftRegIn = 8'hDD; loadShift = 0;  shiftIn = 1; shiftOut = 0;
        SDAdataIn  = 0;
    #5  shiftRegIn = 8'hDD; loadShift = 0;  shiftIn = 1; shiftOut = 0;
        SDAdataIn  = 1;
    #5  shiftRegIn = 8'hDD; loadShift = 0;  shiftIn = 1; shiftOut = 0;
        SDAdataIn  = 1;
    #5 $finish;

end

endmodule
