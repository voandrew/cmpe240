//`timescale 100ns/10ns

module test_mem_bank;

parameter ADDR_WIDTH = 8;
parameter DATA_WIDTH = 8;
parameter BLOCK_WIDTH = 4;

/* instantiate variables */
wire    [DATA_WIDTH-1:0]    d_in_out;
reg     [DATA_WIDTH-1:0]    d_input;
reg     [DATA_WIDTH-1:0]    d_output;
reg             wr_en   = 1;
reg             re_en   = 1;
reg             enable  = 0;
reg             reset   = 1;
reg     [ADDR_WIDTH-1:0]   addr;   // memory has 2^8 (256) addresses
reg     [BLOCK_WIDTH-1:0] block_no;

/* initial conditions */
initial begin
    enable = 1;
    wr_en = 1;
    re_en = 1;
    reset = 1;
    addr = 0;
    d_output <= d_in_out;
end

/* Make a regular pulsi:w
ng clock. */
// always #2 clk = !clk;

// always #20 addr = addr + 1;

initial begin
    $dumpfile("mem_bank.vcd"); // filename of wavefile
    $dumpvars(0,test_mem_bank);    // dump instance m1
end

/* instantiate module */
mem_bank m1(.data_in_out(d_in_out),
            .block_no(block_no),
            .addr_block(addr),
            .enable(enable),
            .read_en(re_en),
            .write_en(wr_en),
            .reset(reset));

wire [DATA_WIDTH - 1: 0] d;
assign d_in_out = (enable == 0 && re_en == 1 && wr_en == 0 && reset == 1)? d_input :{DATA_WIDTH{1'bz}};
assign d = d_in_out; 


/* setup console monitor format */
initial begin
/*    $monitor("At time %t, reset = %b, clk = %b, out = %b",
    $time, reset, clk, out);
*/
end

reg [DATA_WIDTH-1:0] d_in_out_exp;
always @ (d_in_out_exp)
begin
    if(d_in_out_exp != d_in_out)
    begin
    $display("\tTest failed @ %d", $time);
    $display("GOT: \t %d", d_in_out);
    $display("EXP: \t %d", d_in_out_exp);
    end
end


/* runtime conditions */
initial
begin
    #0 addr = 2;
    #4 d_input <= 7;
    #1 enable <= 0; wr_en <= 0; re_en <= 1; reset <= 1; block_no <= 4; 
    #5 enable <= 1; wr_en <= 1; re_en <= 1; reset <= 1; block_no <= 4; d_input <= 7;
    $display("%h, %h, %b, %b, %b, %b", addr, d_in_out, enable, wr_en, re_en, reset);

    #0 addr = 2;
    #4 d_input <= 7;
    #1 enable <= 0; wr_en <= 1; re_en <= 0; reset <= 1; block_no <= 4; 
    #5 enable <= 1; wr_en <= 1; re_en <= 1; reset <= 1; block_no <= 4; 
    $display("%h, %h, %b, %b, %b, %b", addr, d_in_out, enable, wr_en, re_en, reset);


    #0 addr = 4;
    #4 d_input <= 4;
    #1 enable <= 0; wr_en <= 0; re_en <= 1; reset <= 1; block_no <= 4; 
    #5 enable <= 1; wr_en <= 1; re_en <= 1; reset <= 1; block_no <= 4; d_input <= 6;
    $display("%h, %h, %b, %b, %b, %b", addr, d_in_out, enable, wr_en, re_en, reset);

    #0 addr = 4;
    #4 d_input <= 7;
    #1 enable <= 0; wr_en <= 1; re_en <= 0; reset <= 1; block_no <= 4; 
    #5 enable <= 1; wr_en <= 1; re_en <= 1; reset <= 1; block_no <= 4; 
    $display("%h, %h, %b, %b, %b, %b", addr, d_in_out, enable, wr_en, re_en, reset);

    // // testing a write when not enabled
    #1 addr = 3;
    #4 d_input <= 7;
    #1 enable <= 1; wr_en <= 0; re_en <= 1; reset <= 1; block_no <= 4; 
    #5 enable <= 1; wr_en <= 1; re_en <= 1; reset <= 1; block_no <= 4; d_input <= 4;
    $display("%h, %h, %b, %b, %b, %b", addr, d_in_out, enable, wr_en, re_en, reset);

    // // reading memory. output should be 0 since previous wasn't written
    #0 addr = 3;
    #4 d_input <= 7;
    #1 enable <= 0; wr_en <= 1; re_en <= 0; reset <= 1; block_no <= 4; 
    #5 enable <= 1; wr_en <= 1; re_en <= 1; reset <= 1; block_no <= 4; 
    $display("%h, %h, %b, %b, %b, %b", addr, d_in_out, enable, wr_en, re_en, reset);

    #1 addr = 3;
    #4 d_input <= 8;
    #1 enable <= 1; wr_en <= 0; re_en <= 1; reset <= 1; block_no <= 4; 
    #5 enable <= 1; wr_en <= 1; re_en <= 1; reset <= 1; block_no <= 4; d_input <= 1;
    $display("%h, %h, %b, %b, %b, %b", addr, d_in_out, enable, wr_en, re_en, reset);

    #0 addr = 5;
    #4 d_input <= 8;
    #1 enable <= 0; wr_en <= 0; re_en <= 1; reset <= 1; block_no <= 4; 
    #5 enable <= 1; wr_en <= 1; re_en <= 1; reset <= 1; block_no <= 4; d_input <= 7;
    $display("%h, %h, %b, %b, %b, %b", addr, d_in_out, enable, wr_en, re_en, reset);

    #0 addr = 5;
    #4 d_input <= 8;
    #1 enable <= 0; wr_en <= 1; re_en <= 0; reset <= 1; block_no <= 4; 
    #5 enable <= 1; wr_en <= 1; re_en <= 1; reset <= 1; block_no <= 4; 
    $display("%h, %h, %b, %b, %b, %b", addr, d_in_out, enable, wr_en, re_en, reset);
    //
    /*
    #0 d_in_out_exp <= {DATA_WIDTH{1'bz}};
    #0 addr = 0;
    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;    block_no <= 4;  d_input  <= 8'b00000011;        // don't write data
       d_in_out_exp <= {DATA_WIDTH{1'bz}};

    #5 wr_en <= 0;      re_en <= 1;     enable <= 0;    block_no <= 4;  d_input  <= 8'b00000011;        // write data
       d_in_out_exp <= 8'b00000011;

    #5 wr_en <= 1;                                                      d_input  <= 8'b00000010;        // raise wr_en to complete write
       d_in_out_exp <= 8'b00000010;

    #5 wr_en <= 1;      re_en <= 0;
       d_in_out_exp <= 8'b00000011;

    #5 wr_en <= 1;      re_en <= 1;

    // make sure enable works
    #0 addr = 1;
    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;    block_no <= 5;  d_input  <= 8'b00001011;        // don't write data
       d_in_out_exp <= 8'b00001011;

    #5 wr_en <= 0;      re_en <= 1;     enable <= 0;    block_no <= 5;  d_input  <= 8'b00001011;        // write data
       d_in_out_exp <= 8'b00001011;

    #5 wr_en <= 1;                                                      d_input  <= 8'b00001010;        // raise wr_en to complete write
    #5 wr_en <= 1;      re_en <= 0;     enable <= 1;                                                    // should be Z
       d_in_out_exp <= {DATA_WIDTH{1'bz}};

    #5 wr_en <= 1;      re_en <= 1;
    #5 wr_en <= 1;      re_en <= 0;     enable <= 0;                                                    // valid data read
       d_in_out_exp <= 8'b00001011;
   */

    // problem with test below is that WR_EN isn't being raised (POSEDGE REQUIRED)
    /*
    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;    block_no <= 4;  d_input  <= 8'b00000001;        // don't write data
    #5 wr_en <= 0;      re_en <= 1;     enable <= 0;    block_no <= 4;  d_input  <= 8'b00000010;        // write data
    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;    block_no <= 4;  //d_input  <= {DATA_WIDTH{1'bz}};   // don't read data <- required..
    #5 wr_en <= 1;      re_en <= 0;     enable <= 0;    block_no <= 4;  //d_input  <= {DATA_WIDTH{1'bz}};   // read data
    #5 wr_en <= 1;      re_en <= 0;     enable <= 0;    block_no <= 5;  //d_input  <= {DATA_WIDTH{1'bz}};   // read wrong bank data
    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;    block_no <= 5;  //d_input  <= {DATA_WIDTH{1'bz}};   // stop reading <- if deleted, won't write.

    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;    block_no <= 5;  d_input  <= 8'b00000100;        // don't write data
    #5 wr_en <= 0;      re_en <= 1;     enable <= 0;    block_no <= 5;  d_input  <= 8'b00001000;        // write data
    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;    block_no <= 5;  //d_input  <= {DATA_WIDTH{1'bz}};   // don't read data <- required..
    #5 wr_en <= 1;      re_en <= 0;     enable <= 0;    block_no <= 5;  //d_input  <= {DATA_WIDTH{1'bz}};   // read data
    #5 wr_en <= 1;      re_en <= 0;     enable <= 0;    block_no <= 6;  //d_input  <= {DATA_WIDTH{1'bz}};   // read wrong bank data

    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;    block_no <= 6;  d_input  <= 8'b00010000;        // don't write data
    #5 wr_en <= 0;      re_en <= 1;     enable <= 0;    block_no <= 6;  d_input  <= 8'b00100000;        // write data
    #5 wr_en <= 1;      re_en <= 0;     enable <= 0;    block_no <= 6;  //d_input  <= {DATA_WIDTH{1'bz}};   // read data
    #5 wr_en <= 1;      re_en <= 0;     enable <= 0;    block_no <= 7;  //d_input  <= {DATA_WIDTH{1'bz}};   // read wrong bank data

    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;    block_no <= 7;  d_input  <= 8'b01000000;        // don't write data
    #5 wr_en <= 0;      re_en <= 1;     enable <= 0;    block_no <= 7;  d_input  <= 8'b10000000;        // write data
    #5 wr_en <= 1;      re_en <= 0;     enable <= 0;    block_no <= 7;  //d_input  <= {DATA_WIDTH{1'bz}};   // read data
    #5 wr_en <= 1;      re_en <= 0;     enable <= 0;    block_no <= 8;  //d_input  <= {DATA_WIDTH{1'bz}};   // read wrong bank data

    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;    block_no <= 8;  d_input  <= 8'b00000000;        // don't write data
    #5 wr_en <= 0;      re_en <= 1;     enable <= 0;    block_no <= 8;  d_input  <= 8'b11111111;        // write data
    #5 wr_en <= 1;      re_en <= 0;     enable <= 0;    block_no <= 8;  //d_input  <= {DATA_WIDTH{1'bz}};   // read data
    #5 wr_en <= 1;      re_en <= 0;     enable <= 0;    block_no <= 9;  //d_input  <= {DATA_WIDTH{1'bz}};   // read wrong bank data
    */

    #5 $finish;   // finish 5 time units after last instruction
end
endmodule
