module i2c_w_controller (
    output wire shiftEnable,
    output reg loadAddrLSB, loadAddrMSB, incrAddr,
    output reg selAddr, sel5555, selAAAA,
    output reg selData, selAA, sel55, selB0, selC0, selD0, selE0, enDataOut,
    output reg nEn, nWe,
    output reg enSdaOut,
    output reg loadDevId,
    output reg selAux,
    input wire scl, start, stop, writeBit
);
    parameter ADDR_WIDTH = 16;

    wire nSCL;
    assign nSCL = !scl;

    assign shiftEnable = (!enSdaOut && state != S_IDLE ? 1'b1 : 1'b0);

    parameter PREAM_ADDR5           = 16'h5555;
    parameter PREAM_ADDRA           = 16'hAAAA;

    // use this to trigger counts for i2c i/o
    reg [3:0] i2cCount;
    reg [3:0] i2cCountTarget;
    reg i2cCountEnable;
    reg i2cCountHit;

    // use this to trigger counts for preamble stuff
    reg [3:0] ctrlCount;
    reg [3:0] ctrlCountTarget;
    reg ctrlCountEnable;
    reg ctrlCountHit;

    initial begin
        i2cCount        <= 0;
        i2cCountTarget  <= 0;
        i2cCountHit     <= 0;
        i2cCountEnable  <= 1;

        ctrlCount       <= 0;
        ctrlCountTarget <= 0;
        ctrlCountHit    <= 0;
        ctrlCountEnable <= 1;
        
        nEn             <= 1;   nWe <= 1;
        loadDevId       <= 0;   selAux <= 0;
        // addr register
        loadAddrLSB     <= 0; loadAddrMSB <= 0; incrAddr  <= 0;

        // addr mux
        selAddr         <= 0; sel5555     <= 0; selAAAA   <= 0;

        // data mux
        selAA           <= 0; sel55       <= 0; selB0     <= 0; 
        selC0           <= 0; selD0       <= 0; selE0     <= 0;
        selData         <= 0; enSdaOut    <= 0; enDataOut <= 0;
    end

    // state machine
    reg [3:0] state = S_IDLE;
    parameter S_IDLE        = 0 ;
    parameter S_DEV_ID      = 1 ;
    parameter S_WRITE_BIT   = 2 ;
    parameter S_ACK         = 3 ;
    parameter S_ADDR_1      = 5 ;
    parameter S_ADDR_ACK_1  = 6 ;
    parameter S_ADDR_2      = 7 ;
    parameter S_ADDR_ACK_2  = 8 ;
    parameter S_DATA        = 9 ;
    parameter S_DATA_ACK    = 10;
    parameter S_STOP_WRITE  = 11;

    always @ (stop) begin
        if(state > S_IDLE && stop) begin
            i2cCount <= 0;
            i2cCountTarget <= 5;
            state <= S_STOP_WRITE;
            enSdaOut <= 0; 
            selData <= 0; enDataOut <= 0; selAddr <= 0;
            nEn <= 1;   nWe <= 1;
            incrAddr <= 0;
        end
    end

    always @(posedge nSCL or start)
    begin
        if(i2cCount === (i2cCountTarget - 1)) begin
            i2cCountHit <= 1;
            i2cCount    <= 0;
        end
        else begin
            i2cCountHit <= 0;
        end
        if(i2cCountEnable) begin
            i2cCount    <= i2cCount + 1;
        end

        if(ctrlCount === (ctrlCountTarget - 1)) begin
            ctrlCountHit <= 1;
            ctrlCount    <= 0;
        end
        else begin
            ctrlCountHit <= 0;
        end
        if(ctrlCountEnable) begin
            ctrlCount    <= ctrlCount + 1;
        end

        // WARNING: MEALY MACHINE
        case(state)
            S_IDLE: begin
                i2cCount <= 0;
                i2cCountTarget <= 8;    // set to 8 due to timing issue..
                if(start) begin
                    state <= S_DEV_ID;
                end
                else begin 
                    state <= S_IDLE;
                end
            end
            S_DEV_ID: begin
                // $display("GETTING DEV ID");
                if(i2cCountHit) begin 
                    state <= S_WRITE_BIT;
                end
                else begin 
                    state <= S_DEV_ID;
                end
            end
            S_WRITE_BIT: begin
                // check MSB of shiftreg
                i2cCountTarget <= 8;
                if(writeBit == 0) begin
                    loadDevId <= 1;
                    state <= S_ACK;
                    enSdaOut <= 1;
                    ctrlCount       <= 0; 
                    ctrlCountTarget <= 7;
                end
                else begin
                    // $display("NO WRITE BIT: %b", writeBit);
                    state <= S_IDLE;    // interface only supports write
                end
            end
            S_ACK: begin
                // $display("SEND ACK 1");
                loadDevId       <= 0;
                enSdaOut        <= 0;
                i2cCountTarget  <= 7;
                i2cCount        <= 0;
                state           <= S_ADDR_1;

                // send pream to flash memory
                $display("SENDING AAAA");
                enDataOut       <= 1;
                sel5555         <= 1;   selAA <= 1;
                #2.5
                nEn             <= 0;   nWe   <= 0;
                #2.5
                nEn             <= 1;   nWe   <= 1;
            end
            S_ADDR_1: begin                 // get addr LSB
                // $display("GETTING ADDR LSB");
                loadDevId <= 0;             // deassert load devid from last state
                i2cCountTarget <= 7;        // look for next 8 bits
                enSdaOut <= 0;              // deassert ACK
                enDataOut       <= 0;
                sel5555         <= 0;   selAA <= 0;
                nEn             <= 1;   nWe   <= 1;
                if(i2cCountHit) begin       // check i2cCountEr
                    state <= S_ADDR_ACK_1;
                    selAAAA <= 0; sel5555 <= 0;
                    sel55   <= 0; selAA   <= 0;
                    selB0   <= 0; selC0   <= 0;
                end
                else begin
                    state <= S_ADDR_1;

                    // check even cycle numbers for command signals
                    if(ctrlCount[0] === 0) begin 
                        case(ctrlCount)
                            2: begin
                                $display("SENDING AAAA");
                                enDataOut <= 1;
                                selAAAA   <= 1;
                                sel55     <= 1;
                                #2.5  nEn <= 0; nWe <= 0;
                                #2.5  nEn <= 1; nWe <= 1;
                            end
                            4: begin
                                $display("SENDING 5555");
                                enDataOut <= 1;
                                sel5555   <= 1;
                                selB0     <= 1;
                                #2.5  nEn <= 0; nWe <= 0;
                                #2.5  nEn <= 1; nWe <= 1;
                            end
                            6: begin
                                enDataOut <= 1;
                                selC0     <= 1;
                                #2.5  nEn <= 0; nWe <= 0;
                                #2.5  nEn <= 1; nWe <= 1;
                            end
                        endcase // ctrlCount
                    end
                    // otherwise HOLD
                    else begin
                        nEn <= 1;
                        nWe <= 1;
                        enDataOut<= 0;
                        selAAAA <= 0;
                        sel5555 <= 0;
                        sel55   <= 0;
                        selAA   <= 0;
                        selB0   <= 0;
                        selC0   <= 0;
                    end
                end
            end
            S_ADDR_ACK_1: begin
                // $display("SEND ACK LSB");
                loadAddrLSB <= 1;
                enSdaOut <= 1;              // send ACK
                i2cCount <= 0;
                state <= S_ADDR_2;
                selC0 <= 0;
            end
            S_ADDR_2: begin                 // get addr MSB
                // $display("GETTING ADDR MSB");
                loadAddrLSB <= 0;
                i2cCountTarget <= 7;
                enSdaOut <= 0;              // deassert ACK
                if(i2cCountHit) begin        // check i2cCountEr
                    state <= S_ADDR_ACK_2;
                end
                else begin
                    state <= S_ADDR_2;
                end
            end
            S_ADDR_ACK_2: begin
                // $display("SEND ACK MSB");
                loadAddrMSB <= 1;
                enSdaOut <= 1;              // send ACK
                i2cCount <= 0;
                state <= S_DATA;
            end
            S_DATA: begin
                incrAddr <= 0;
                loadAddrMSB <= 0;
                enDataOut <= 0;
                enSdaOut <= 0;              // deassert ACK
                nEn <= 1;   nWe <= 1;       // deassert continue mode (from ACK)
                selC0 <= 0;
                if(stop) begin
                    i2cCount <= 0;
                    i2cCountTarget <= 5;
                    state <= S_STOP_WRITE;
                    enSdaOut <= 0; 
                    selData <= 0; enDataOut <= 0; selAddr <= 0;
                    nEn <= 1;   nWe <= 1;
                    incrAddr <= 0;
                end
                else begin
                    // check i2cCountEr
                    i2cCountTarget <= 7;
                    if(i2cCountHit) begin
                        state <= S_DATA_ACK;
                        enSdaOut <= 1;              // send ACK
                        // write data also
                        selData <= 1;   enDataOut <= 1; selAddr <= 1;
                        #2.5  nEn <= 0; nWe <= 0;
                        #2.5  nEn <= 1; nWe <= 1;
                        incrAddr <= 1;
                    end
                    else begin
                        state <= S_DATA;

                        selData <= 0;
                        nEn <= 1;   nWe <= 1;
                    end

                end
            end
            S_DATA_ACK: begin
                enSdaOut <= 0; 
                selData <= 0; enDataOut <= 0; selAddr <= 0;
                nEn <= 1;   nWe <= 1;
                incrAddr <= 0;
                i2cCount <= 0;
                if(stop) begin
                    i2cCount <= 0;
                    i2cCountTarget <= 5;
                    state <= S_STOP_WRITE;
                    enSdaOut <= 0; 
                    selData <= 0; enDataOut <= 0; selAddr <= 0;
                    nEn <= 1;   nWe <= 1;
                    incrAddr <= 0;
                end
                else begin
                    // go back to continue mode
                    enDataOut <= 1;
                    selC0 <= 1;
                    #2.5  nEn <= 0; nWe <= 0;
                    #2.5  nEn <= 1; nWe <= 1;
                    state <= S_DATA;
                end
            end
            S_STOP_WRITE: begin
                incrAddr <= 0;
                enSdaOut <= 0;              // deassert ACK
                nEn <= 1;   nWe <= 1;
                selData <= 0;
                selAux <= 1;
                // get out of write and go to S_IDLE
                // check even cycle numbers for command signals
                if(i2cCountHit) begin
                    state   <= S_IDLE;
                    selAux  <= 0;
                    // write data also
                end
                else begin
                    if(i2cCount[0] === 1) begin 
                        nEn   <= 0; nWe     <= 0;
                        case(i2cCount)
                            1: begin
                                enDataOut <= 1;
                                selD0   <= 1;
                                #2.5  nEn <= 0; nWe <= 0;
                                #2.5  nEn <= 1; nWe <= 1;
                            end
                            3: begin
                                enDataOut <= 1;
                                selE0   <= 1;
                                #2.5  nEn <= 0; nWe <= 0;
                                #2.5  nEn <= 1; nWe <= 1;
                            end
                        endcase // ctrlCount
                    end
                    // otherwise HOLD
                    else begin
                        nEn   <= 1; nWe     <= 1;
                        enDataOut <= 0;
                        selD0 <= 0; selE0   <= 0;
                    end
                end
            end
            default: begin
                state <= S_IDLE;
            end
        endcase // state
    end
endmodule // i2c_w_data_mux