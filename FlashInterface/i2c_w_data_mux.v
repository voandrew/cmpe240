module i2c_w_data_mux(dataIo, dataInput, selData, selAA, sel55, selB0, selC0, selD0, selE0, enDataOut, scl); 
    parameter DATA_WIDTH = 8;
    inout [DATA_WIDTH - 1: 0] dataIo;
    input [DATA_WIDTH - 1: 0] dataInput;
    input wire selData, selAA, sel55, selB0, selC0, selD0, selE0, enDataOut, scl;

    wire [6:0] dataMuxSelector;
    reg [DATA_WIDTH - 1: 0] dataOutput;
    wire nSCL;


    parameter CODE_WRITE            = 8'h20;
    parameter CODE_FASTWRITE_SET    = 8'hB0;
    parameter CODE_FASTWRITE_CONT   = 8'hC0;
    parameter CODE_FASTWRITE_RST1   = 8'hD0;
    parameter CODE_FASTWRITE_RST2   = 8'hE0;

    parameter PREAM_ADDR5           = 16'h5555;
    parameter PREAM_ADDRA           = 16'hAAAA;
    parameter PREAM_DATA5           = 8'h55;
    parameter PREAM_DATAA           = 8'hAA;

    assign nSCL = !scl;
    assign dataIo = (enDataOut ? dataOutput : {DATA_WIDTH{1'bz}});
    assign dataMuxSelector[6] = selE0;
    assign dataMuxSelector[5] = selD0;
    assign dataMuxSelector[4] = selC0;
    assign dataMuxSelector[3] = selB0;
    assign dataMuxSelector[2] = sel55;
    assign dataMuxSelector[1] = selAA;
    assign dataMuxSelector[0] = selData;


    always @(*)
    begin
        case (dataMuxSelector)
            7'b0000001: begin 
                dataOutput <= dataInput;
            end
            7'b0000010: begin 
                dataOutput <= PREAM_DATAA;
            end
            7'b0000100: begin 
                dataOutput <= PREAM_DATA5;
            end
            7'b0001000: begin 
                dataOutput <= CODE_FASTWRITE_SET;
            end
            7'b0010000: begin 
                dataOutput <= CODE_FASTWRITE_CONT;
            end
            7'b0100000: begin 
                dataOutput <= CODE_FASTWRITE_RST1;
            end
            7'b1000000: begin 
                dataOutput <= CODE_FASTWRITE_RST2;
            end
            default : dataOutput <= dataInput;
        endcase
    end
endmodule // i2c_w_data_mux