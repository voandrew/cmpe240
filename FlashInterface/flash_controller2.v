module flash_controller(nMemEnable, nMemRead, nMemWrite, nMemReset, address, data, nEnable, nRead, nWrite, nReset, decoderEnable);

parameter ADDR_WIDTH = 16;
parameter DATA_WIDTH = 8;

reg [5:0] state;
parameter S_CMD_PRE_1       = 0;
parameter S_CMD_PRE_2       = 1;
parameter S_CMD_CODE        = 2;
parameter S_READ            = 3;
parameter S_WRITE           = 4;
parameter S_FASTWRITE_PRE   = 5;
parameter S_FASTWRITE_NOW   = 6;
parameter S_FASTWRITE_RST1  = 7;
parameter S_FASTWRITE_RST2  = 8;

parameter CODE_READ             = 8'h00;
parameter CODE_WRITE            = 8'h20;
parameter CODE_FASTWRITE_SET    = 8'hB0;
parameter CODE_FASTWRITE_CONT   = 8'hC0;
parameter CODE_FASTWRITE_RST1   = 8'hD0;
parameter CODE_FASTWRITE_RST2   = 8'hE0;
parameter CODE_RESET            = 8'hF0;

parameter PREAM_ADDR5           = 16'h5555;
parameter PREAM_ADDRA           = 16'hAAAA;
parameter PREAM_DATA5           = 8'h55;
parameter PREAM_DATAA           = 8'hAA;


input wire nEnable, nWrite, nRead, nReset;
input wire [ADDR_WIDTH-1:0] address;
input wire [DATA_WIDTH-1:0] data;
output reg nMemEnable, nMemRead, nMemWrite, nMemReset, decoderEnable;


initial begin
    state = S_CMD_PRE_1;
    nMemEnable <= 1; nMemRead <= 1; nMemWrite <= 1; nMemReset <= 1;
end

always @ (posedge nEnable or posedge nWrite or posedge nRead or posedge nReset)
begin
    nMemEnable <= nEnable;
    nMemWrite <= nWrite;
    nMemRead <= nRead;
    nMemReset <= nReset;
    if(state == S_FASTWRITE_PRE || state == S_FASTWRITE_RST1 || state == S_FASTWRITE_RST2
        || state == S_CMD_CODE || state == S_CMD_PRE_1 || state == S_CMD_PRE_2)
    begin
        decoderEnable <= 0;
    end
end

always @ (negedge nEnable or !nWrite or posedge nWrite or negedge nRead)
begin
    case(state)
        S_CMD_PRE_1:
        begin
            nMemEnable <= 1; nMemWrite <= 1; nMemRead <= 1; nMemReset <= 1;
            if(nWrite == 1 && address == PREAM_ADDR5 && data == PREAM_DATAA)
            begin
                $display("PREAM1 FOUND");
                state <= S_CMD_PRE_2;
            end
            else 
            begin
                state <= S_CMD_PRE_1;
            end
        end
        S_CMD_PRE_2:
        begin
            if(nWrite == 1)
            begin
                if(address == PREAM_ADDRA && data == PREAM_DATA5)
                begin
                    $display("PREAM2 FOUND");
                    state <= S_CMD_CODE;
                end
                else 
                begin
                    // $display("PREAM2 missing %h, %h", address, data);
                    state <=S_CMD_PRE_1;
                end
            end
        end
        S_CMD_CODE:
        begin
            if(address == PREAM_ADDR5)
            begin
                // $display("COMMAND CODE... %h", data);
                case(data)
                    CODE_READ:
                    begin 
                        state <= S_READ;
                    end
                    CODE_WRITE:
                    begin 
                        state <= S_WRITE;
                    end
                    CODE_FASTWRITE_SET:
                    begin 
                        state <= S_FASTWRITE_PRE;
                    end
                endcase // data
            end
            else begin 
                // $display("BAD PREAMBLE... %h (%h)", address, PREAM_ADDR5);
                state <= S_CMD_PRE_1;
            end
        end
        S_READ:
        begin
            if(nEnable == 0 && nWrite == 1 && nRead == 0 && nReset == 1)    // negedge nRead
            begin
                // $display("READ TRIGGERED @ %h", address);
                nMemEnable <= 0; nMemWrite <= 1; nMemRead <= 0; nMemReset <= 1;
                decoderEnable <= 1;
                state <= S_CMD_PRE_1;
            end
        end
        S_WRITE:
        begin
            if(nWrite == 0) // trigger action on negative level of nWrite
            begin
                // $display("WRITE TRIGGERED @ %h", address);
                nMemEnable <= 0; nMemWrite <= 0; nMemRead <= 1; nMemReset <= 1;
                decoderEnable <= 1;
                state <= S_CMD_PRE_1;
            end
        end
        S_FASTWRITE_PRE:
        begin
            nMemEnable <= 1; nMemWrite <= 1; nMemRead <= 1; nMemReset <= 1;
            if(nWrite == 1)
            begin
                if(data == CODE_FASTWRITE_CONT)
                begin
                    $display("FASTWRITE CONTINUE");
                    state <= S_FASTWRITE_NOW;
                end
                else if (data == CODE_FASTWRITE_RST1)
                begin
                    state <= S_FASTWRITE_RST1;
                end
            end
        end
        S_FASTWRITE_NOW:
        begin
            if(nWrite == 0) // trigger action on negative level of nWrite
            begin
                // $display("FASTWRITE %h @ %h", data, address);
                nMemEnable <= 0; nMemWrite <= 0; nMemRead <= 1; nMemReset <= 1;
                decoderEnable <= 1;
                state <= S_FASTWRITE_PRE;
            end
        end
        S_FASTWRITE_RST1:
        begin
            if(nWrite == 1)
            begin
                if(data == CODE_FASTWRITE_RST2)
                begin
                    // $display("FASTWRITE RESET");
                    state <= S_CMD_PRE_1;
                end
            end
        end
        default:
        $display("Shouldn't be here. State: %d", state);
    endcase
end
endmodule