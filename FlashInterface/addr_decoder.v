module addr_decoder(decoderEnable, address, bankselect, blockselect, rowselect);
parameter ADDR_WIDTH        = 16;
parameter BANK_SEL_WIDTH    = 16;
parameter BLOCK_SEL_WIDTH   = 4;
parameter ROW_SEL_WIDTH     = 8;

output reg [BANK_SEL_WIDTH-1 : 0] bankselect;
output reg [BLOCK_SEL_WIDTH-1: 0] blockselect;
output reg [ROW_SEL_WIDTH  -1: 0] rowselect;
input wire [ADDR_WIDTH-1: 0]      address;
input wire decoderEnable;

always@(*)
begin
    if(decoderEnable)
    begin
        case(address[15:12])
            0: bankselect = 16'h0001;
            1: bankselect = 16'h0002; 
            2: bankselect = 16'h0004;
            3: bankselect = 16'h0008;

            4: bankselect = 16'h0010;
            5: bankselect = 16'h0020;
            6: bankselect = 16'h0040;
            7: bankselect = 16'h0080;

            8: bankselect = 16'h0100;
            9: bankselect = 16'h0200;
           10: bankselect = 16'h0400;
           11: bankselect = 16'h0800;

           12: bankselect = 16'h1000;
           13: bankselect = 16'h2000;
           14: bankselect = 16'h4000;
           15: bankselect = 16'h8000;
        endcase // address[15:12]
        blockselect = ~address[11:8];
        rowselect   = address[7:0];
    end
    else 
    begin
        bankselect  = 16'hzzzz;
        blockselect = 4'hz;
        rowselect   = 8'hzz;
    end
end
endmodule
