# Flash Memory Interface

> WIP

## Modules

### Flash Core

* consists of banks/blocks
* addressable by row/col

### Controller

* User inputs are to activate the command mode of the controller and also to:
    + release data from the bidirectional IO
    + ???

```
        /           \                         /         \ 
        | nEN       |                         | memEN   |
USER -->| nRE       |-----> CONTROLLER -----> | memRE   |---> MEM
        | nWE       |                         | memWE   |
        | nRESET    |                         | memReset|
        \           /                         \         /
```
