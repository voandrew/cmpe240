module i2c_w_addr_mux(addrOut, addrIn, selAddr, selAAAA, sel5555, scl);
    parameter ADDR_WIDTH = 16;
    output reg [ADDR_WIDTH - 1: 0] addrOut;
    input wire [ADDR_WIDTH - 1: 0] addrIn;
    input wire selAddr, selAAAA, sel5555, scl;

    wire nSCL;
    assign nSCL = !scl;

    parameter PREAM_ADDR5           = 16'h5555;
    parameter PREAM_ADDRA           = 16'hAAAA;

    wire [2:0] addrMuxSel;
    assign addrMuxSel[2] = sel5555;
    assign addrMuxSel[1] = selAAAA;
    assign addrMuxSel[0] = selAddr;


    always @(*)
    begin
        case (addrMuxSel)
            3'b001: begin 
                addrOut <= addrIn;
            end
            3'b010: begin 
                addrOut <= PREAM_ADDRA;
            end
            3'b100: begin 
                addrOut <= PREAM_ADDR5;
            end
            default : addrOut <= {ADDR_WIDTH{1'bz}};
        endcase
    end
endmodule // i2c_w_data_mux