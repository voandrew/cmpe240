//`timescale 100ns/10ns

module test_mem_block;

parameter ADDR_WIDTH = 8;
parameter DATA_WIDTH = 8;

/* instantiate variables */
wire    [DATA_WIDTH-1:0]    d_in_out;
reg     [DATA_WIDTH-1:0]    d_input;
reg             wr_en   = 1;
reg             re_en   = 1;
reg             enable  = 0;
reg             reset   = 1;
reg     [ADDR_WIDTH-1:0]   addr;   // memory has 2^8 (256) addresses

/* initial conditions */
initial begin
    wr_en = 0;
    addr = 0;
end

/* Make a regular pulsing clock. */
// always #2 clk = !clk;

// always #20 addr = addr + 1;

initial begin
    $dumpfile("mem_block.vcd"); // filename of wavefile
    $dumpvars(0,m1);    // dump instance m1
end

/* instantiate module */
mem_block m1(.data_in_out(d_in_out),
            .addr(addr),
            .enable(enable),
            .read_en(re_en),
            .write_en(wr_en),
            .reset(reset));

assign d_in_out = (enable == 0 && re_en == 1 && wr_en == 0 && reset == 1)? d_input :{DATA_WIDTH{1'bz}};

/* setup console monitor format */
initial begin
/*    $monitor("At time %t, reset = %b, clk = %b, out = %b",
    $time, reset, clk, out);
*/
end


/* runtime conditions */
initial
begin
    #0 addr = 0;
    #0 wr_en <= 1;      re_en <= 1;     enable <= 0;    d_input  <= 8'b00000001;    // don't write data
    #5 wr_en <= 0;      re_en <= 1;     enable <= 0;    d_input  <= 8'b00000010;    // write data
    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;                            // don't read data
    #5 wr_en <= 1;      re_en <= 0;     enable <= 0;                            // read data

    #5 addr = 1;
    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;    d_input  <= 8'b00000100;    // don't write data
    #5 wr_en <= 0;      re_en <= 1;     enable <= 0;    d_input  <= 8'b00001000;    // write data
    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;                            // don't read data
    #5 wr_en <= 1;      re_en <= 0;     enable <= 0;                            // read data
    #5 addr = 2;
    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;    d_input  <= 8'b00010000;    // don't write data
    #5 wr_en <= 0;      re_en <= 1;     enable <= 0;    d_input  <= 8'b00100000;    // write data
    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;                            // don't read data
    #5 wr_en <= 1;      re_en <= 0;     enable <= 0;                            // read data
    #5 addr = 3;
    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;    d_input  <= 8'b01000000;    // don't write data
    #5 wr_en <= 0;      re_en <= 1;     enable <= 0;    d_input  <= 8'b10000000;    // write data
    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;                            // don't read data
    #5 wr_en <= 1;      re_en <= 0;     enable <= 0;                            // read data
    #5 addr = 4;
    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;    d_input  <= 8'b00000000;    // don't write data
    #5 wr_en <= 0;      re_en <= 1;     enable <= 0;    d_input  <= 8'b11111111;    // write data
    #5 wr_en <= 1;      re_en <= 1;     enable <= 0;                            // don't read data
    #5 wr_en <= 1;      re_en <= 0;     enable <= 0;                            // read data
    #200 $finish;   // run for 200 time units
end
endmodule
