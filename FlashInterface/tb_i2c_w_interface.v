`timescale 100ns/10ns

module test_i2c_w_interface;
/* instantiate variables */
parameter ADDR_WIDTH = 16;
parameter DATA_WIDTH = 8;

// i2c signals
reg sclMaster;
reg sclAux;
reg sda;

wire sdaReal;

pullup pup(sdaReal);
nmos(sdaReal, 1'b0, !sda);
initial begin
    sclMaster <= 1;
    sclAux <= 1;
    sda <= 1;
    #2 
    sda <= 0;   // START
end

// module flash_module(data_in_out, address, nEnable, nRead, nWrite, nReset);
inout [DATA_WIDTH - 1 : 0] dataIo;
wire  [ADDR_WIDTH - 1 : 0 ] addrOut;


flash_module fm1(
    .data_in_out(dataIo),
    .address(addrOut),
    .nEnable    (nEn),
    .nWrite     (nWe)
    );

i2c_w_interface i2cWriteInterface(
    .dataIo   (dataIo),
    .addrOut  (addrOut),
    .sclMaster(sclMaster),
    .sclAux   (sclAux),
    .sda       (sdaReal),
    .nEn        (nEn),
    .nWe        (nWe)
    );

initial begin
    $dumpfile("i2cWriteInterface.vcd"); // filename of wavefile
    $dumpvars(0,test_i2c_w_interface);    // dump instance m1
end


/* setup test condition */
// always @ (addrOut)
// begin
//     //$display("\n\t-----------");
//     $display("\n\t----------------------");
//     if(addrOut !== addrOut_ex)
//     begin
//         $display("\tMUX OUTPUT ERROR");
//     end
//     else
//     begin
//         $display("\tMUX OUTPUT OK");
//     end
//     $display("\t----------------------");
//     $display("\tEXPECTED: 0x%h", addrOut_ex);
//     $display("\tRECEIVED: 0x%h", addrOut);
// end

parameter CODE_READ             = 8'h00;
parameter CODE_WRITE            = 8'h20;
parameter CODE_FASTWRITE_SET    = 8'hB0;
parameter CODE_FASTWRITE_CONT   = 8'hC0;
parameter CODE_FASTWRITE_RST1   = 8'hD0;
parameter CODE_FASTWRITE_RST2   = 8'hE0;
parameter CODE_RESET            = 8'hF0;

parameter PREAM_ADDR5           = 16'h5555;
parameter PREAM_ADDRA           = 16'hAAAA;
parameter PREAM_DATA5           = 8'h55;
parameter PREAM_DATAA           = 8'hAA;
/* initial conditions */

/* Make a regular pulsing clock. */
reg killclock = 0;
parameter CLOCK_PERIOD = 10;
always begin
    if(killclock === 0)
    begin
        #(CLOCK_PERIOD/2) sclMaster <= !sclMaster;
    end
    #(CLOCK_PERIOD/2) sclAux <= !sclAux;
end
reg [11:0] count = 0;
always @(negedge sclMaster)
begin 
    count <= count + 1;
    sda <= count[3];
    if(count == 90)
    begin
        $display("KILL");
        killclock <= 0;
        sclMaster <= 1;
        sda <= 0;
        #2
        sda <= 1;
    end

end
/* runtime conditions */
initial
begin
    #2000
    $finish;
end
endmodule