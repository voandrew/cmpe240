module mem_block(data_in_out, addr, enable, read_en, write_en, reset);
    parameter ADDR_WIDTH = 8;
    parameter DATA_WIDTH = 8;

    // bidirectional data bus
    inout wire[DATA_WIDTH-1:0] data_in_out;
    output reg[DATA_WIDTH-1:0] data_out = {DATA_WIDTH{1'bz}};
    wire [DATA_WIDTH-1:0] data_in = {DATA_WIDTH{1'bz}};


    // address
    input wire[ADDR_WIDTH-1:0] addr;

    // memory
    reg [DATA_WIDTH-1:0] flash_core_block [(2**ADDR_WIDTH)-1:0];

    // Controll signals
    input wire enable, read_en, write_en, reset;

    integer i;
    initial
    begin
        for(i = 0; i < 2**ADDR_WIDTH; i = i + 1)
        begin
            flash_core_block[i] <= 0;
        end
    end


    // Read assignment
    // Will lead floating if this is a read
    // ENABLE = 0   WRITE_EN = 1    READ_EN = 0     RESET = 1
    assign data_in_out = (!enable && write_en && !read_en && reset)? data_out : {DATA_WIDTH{1'bz}} ;
    // assign data_in = (!enable && !write_en && read_en && reset)? data_in_out : {DATA_WIDTH{1'bz}} ;

    // Write -- write activates on posedge write enable
    always@(negedge enable or posedge write_en or posedge read_en or posedge reset)
    begin
        if(!enable && read_en && reset)
        begin
            // $display("%b, %b, %b, %b", enable, write_en, read_en, reset);
            // $display("WRITING %h@%h", data_in_out, addr);
            flash_core_block[addr] <= data_in_out;
        end
    end

    always@(negedge read_en or negedge enable)
    begin
        if(enable === 0 && write_en == 1 && read_en === 0 && reset === 1)
        begin
            // $display("READING %h@%h", flash_core_block[addr], addr);
            data_out = flash_core_block[addr];
        end
    end
endmodule
