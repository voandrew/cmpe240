`timescale 100ns/10ns
module tb_i2c_start_stop_detector;
    reg scl;
    reg sda;

    i2c_start_stop_detector i2c_detector(
        .scl  (scl),
        .sda  (sda),
        .start(start),
        .stop (stop)
        );

    initial begin
        $dumpfile("i2c_detector.vcd"); // filename of wavefile
        $dumpvars(0,i2c_detector);    // dump instance m1
    end

    initial begin
        scl <= 1;
        sda <= 1;
    end

    parameter DELAY = 5;
    initial begin
        #DELAY
        sda <= 1;
        #DELAY
                    sda <= 0;  // start condition
        #DELAY
        scl <= 0;
        #DELAY
        sda = 1;
        #1 scl = 1;
        // scl <= 1;   sda <= 1;  // should not be stop condition
        #DELAY
        scl <= 1;
        #DELAY
        scl <= 0;   sda <= 0;
        #DELAY
        scl <= 1;   sda <= 0;
        #DELAY
        scl <= 1;   sda <= 1;
        #DELAY
        scl <= 1;
        #DELAY
        scl <= 0;   sda <= 0;
        #DELAY
        $finish;
    end
endmodule // tb_i2c_start_stop_detector