`timescale 100ns/10ns

module test_i2c_w_data_mux;

/* instantiate variables */
reg scl;
parameter ADDR_WIDTH = 16;
parameter DATA_WIDTH = 8;

reg selData, selAA, sel55, selB0, selC0, selD0, selE0, enDataOut;
reg [DATA_WIDTH - 1: 0] dataInput;

inout wire    [DATA_WIDTH-1:0] d_in_out;
reg    [DATA_WIDTH-1:0] d_output_ex;
wire     [DATA_WIDTH-1:0] d_output;

assign d_output = d_in_out;

i2c_w_data_mux i2cDataMux(
    .dataIo(d_in_out),
    .dataInput(dataInput),
    .selData(selData),
    .selAA(selAA),
    .sel55(sel55),
    .selB0(selB0),
    .selC0(selC0),
    .selD0(selD0),
    .selE0(selE0),
    .enDataOut(enDataOut),
    .scl(scl)
    );

/* Make a regular pulsing clock. */
parameter CLOCK_PERIOD = 10;
// always #(CLOCK_PERIOD/2) scl <= !scl;

initial begin
    d_output_ex = 8'hzz;
    dataInput = 8'hzz;
end

initial begin
    $dumpfile("i2cDataMux.vcd"); // filename of wavefile
    $dumpvars(0,i2cDataMux);    // dump instance m1
end


/* setup test condition */
always @ (d_output)
begin
    //$display("\n\t-----------");
    $display("\n\t----------------------");
    if(d_output !== d_output_ex)
    begin
        $display("\tMUX OUTPUT ERROR");
    end
    else
    begin
        $display("\tMUX OUTPUT OK");
    end
    $display("\t----------------------");
    $display("\tEXPECTED: 0x%h", d_output_ex);
    $display("\tRECEIVED: 0x%h", d_output);
end

parameter CODE_READ             = 8'h00;
parameter CODE_WRITE            = 8'h20;
parameter CODE_FASTWRITE_SET    = 8'hB0;
parameter CODE_FASTWRITE_CONT   = 8'hC0;
parameter CODE_FASTWRITE_RST1   = 8'hD0;
parameter CODE_FASTWRITE_RST2   = 8'hE0;
parameter CODE_RESET            = 8'hF0;

parameter PREAM_ADDR5           = 16'h5555;
parameter PREAM_ADDRA           = 16'hAAAA;
parameter PREAM_DATA5           = 8'h55;
parameter PREAM_DATAA           = 8'hAA;
/* initial conditions */
initial begin

end

/* runtime conditions */
initial
begin
    dataInput <= 8'h22; 
    selData <= 0; selAA <= 0; sel55 <= 0; selB0 <= 0; selC0 <= 0; selD0 <= 0; selE0 <= 0; enDataOut <= 0;
    d_output_ex <= 8'hZZ;

    #CLOCK_PERIOD 
    dataInput = 8'h49; 
    selData = 1; selAA <= 0; sel55 <= 0; selB0 <= 0; selC0 <= 0; selD0 <= 0; selE0 <= 0; enDataOut <= 1;
    d_output_ex <= 8'h49;

    // check for PREAM AA
    #CLOCK_PERIOD 
    dataInput <= 8'h49; 
    selData <= 0; selAA <= 1; sel55 <= 0; selB0 <= 0; selC0 <= 0; selD0 <= 0; selE0 <= 0; enDataOut <= 1;
    d_output_ex <= 8'hAA;

    #CLOCK_PERIOD 
    dataInput <= 8'h49; 
    selData <= 0; selAA <= 0; sel55 <= 1; selB0 <= 0; selC0 <= 0; selD0 <= 0; selE0 <= 0; enDataOut <= 1;
    d_output_ex <= 8'h55;

    #CLOCK_PERIOD 
    dataInput <= 8'h49; 
    selData <= 0; selAA <= 0; sel55 <= 0; selB0 <= 1; selC0 <= 0; selD0 <= 0; selE0 <= 0; enDataOut <= 1;
    d_output_ex <= 8'hB0;

    #CLOCK_PERIOD 
    dataInput <= 8'h49; 
    selData <= 0; selAA <= 0; sel55 <= 0; selB0 <= 0; selC0 <= 1; selD0 <= 0; selE0 <= 0; enDataOut <= 1;
    d_output_ex <= 8'hC0;

    #CLOCK_PERIOD 
    dataInput <= 8'h49; 
    selData <= 0; selAA <= 0; sel55 <= 0; selB0 <= 0; selC0 <= 0; selD0 <= 1; selE0 <= 0; enDataOut <= 1;
    d_output_ex <= 8'hD0;

    #CLOCK_PERIOD 
    dataInput <= 8'h49; 
    selData <= 0; selAA <= 0; sel55 <= 0; selB0 <= 0; selC0 <= 0; selD0 <= 0; selE0 <= 1; enDataOut <= 1;
    d_output_ex <= 8'hE0;

    #CLOCK_PERIOD 
    dataInput = 8'h85; 
    selData <= 0; selAA <= 0; sel55 <= 0; selB0 <= 0; selC0 <= 0; selD0 <= 0; selE0 <= 0; enDataOut <= 1;
    d_output_ex = 8'h85;

    #CLOCK_PERIOD 
    dataInput <= 8'h85; 
    selData <= 0; selAA <= 0; sel55 <= 0; selB0 <= 0; selC0 <= 1; selD0 <= 0; selE0 <= 0; enDataOut <= 0;
    d_output_ex <= 8'hZZ;

    #20
    $finish;
end
endmodule