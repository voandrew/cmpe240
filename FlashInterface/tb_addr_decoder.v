`timescale 100ns/10ns

module test_address_decoder;

/* instantiate variables */
reg clk;
parameter WIDTH = 16;
reg     [WIDTH - 1:0] address;
wire    [3:0] bankselect;
wire    [3:0] blockselect;
wire    [7:0] rowselect;

// expected
reg    [3:0] bankselect_expect;
reg    [3:0] blockselect_expect;
reg    [7:0] rowselect_expect;

/* instantiate module */
addr_decoder addr_decode1(.address(address),
                    .bankselect(bankselect),
                    .blockselect(blockselect),
                    .rowselect(rowselect)
                );

/* initial conditions */
initial begin
    clk                 = 0;
    address             = 0;
    bankselect_expect   = 0;
    blockselect_expect  = 0;
    rowselect_expect    = 0;
end

/* Make a regular pulsing clock. */
always #2 clk = !clk;

initial begin
    $dumpfile("address_decoder.vcd"); // filename of wavefile
    $dumpvars(0,addr_decode1);    // dump instance m1
end

/* setup console monitor format */
initial begin
    $display("\t\ttime, addr, bank, block, row, bank_expect, block_expect, row_expect");
    $monitor("%d, %b, %b, %b, %b, %b, %b, %b",
            $time, address, bankselect, blockselect, rowselect,
            bankselect_expect, blockselect_expect, rowselect_expect
        );
end

/* setup test condition */
always @ (posedge clk)
begin
    if(bankselect_expect != bankselect) 
    begin
        $display("\nBANK SELECT ERROR");
        $display("error at time %d", $time);
        $display("Expected %d, got value %d", bankselect_expect, bankselect);
    end
    if(blockselect_expect != blockselect) 
    begin
        $display("\nBLOCK SELECT ERROR");
        $display("error at time %d", $time);
        $display("Expected %d, got value %d", blockselect_expect, blockselect);
    end
    if(rowselect_expect != rowselect) 
    begin
        $display("\nROW SELECT ERROR");
        $display("error at time %d", $time);
        $display("Expected %d, got value %d", rowselect_expect, rowselect);
    end
end

/* runtime conditions */
initial
begin
    #4 address <= 16'b0000111100001111; 
       bankselect_expect <= 4'b0000; 
       blockselect_expect <= 4'b1111; 
       rowselect_expect <= 8'b00001111;
    #4 address <= 16'b0100110000001011; 
       bankselect_expect <= 4'b0100; 
       blockselect_expect <= 4'b1100; 
       rowselect_expect <= 8'b00001011;
    #4 address <= 16'b0011110100101010; 
       bankselect_expect <= 4'b0011; 
       blockselect_expect <= 4'b1101;
       rowselect_expect <= 8'b00101010;
    #4 $finish;
end
endmodule 
