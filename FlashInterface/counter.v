
module counter(out, enable, clk, reset);
    parameter   WIDTH = 3;  // 5 bit counter
    output  reg [WIDTH - 1: 0]      out;
    input   wire                    clk, reset, enable;
    initial
    begin
        out = 0;
    end
    
    always@(posedge clk or reset)
    begin
        if(reset)  // asynchronous reset
        begin
            out <= 0;
        end
        else if (enable)
        begin
            out <= out + 1;
        end
        else out <= out;    // avoid a latch
    end
endmodule
