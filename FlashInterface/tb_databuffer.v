`timescale 100ns/10ns

module test_dflipflop;

/* instantiate variables */
parameter WIDTH = 8;
reg     clk = 1;
reg     [WIDTH - 1:0] d;
wire    [WIDTH - 1:0] q;
reg     [WIDTH - 1:0] q_expect;

/* instantiate module */
databuffer db1(.d(d),
            .q(q),
            .clk(clk));

/* initial conditions */
initial begin
    clk = 0;
    d = 0;
end

/* Make a regular pulsing clock. */
always #2 clk = !clk;

initial begin
    $dumpfile("databuffer.vcd"); // filename of wavefile
    $dumpvars(0,db1);    // dump instance m1
end

/* setup console monitor format */
initial begin
    $display("\t\ttime, d, q, q_expect");
    $monitor("%d, %b, %b, %b",
            $time, d, q, q_expect); 
end

/* setup test condition */
always @ (posedge clk)
begin
    if(q_expect != q) 
    begin
        $display("error at time %d", $time);
        $display("Expected %d, got value %d", q_expect, q);
    end
end

/* runtime conditions */
initial
begin
    #4 d <= 1;  q_expect <= 0;
    #4 d <= 15; q_expect <= 1;
    #4 d <= 10; q_expect <= 15;
    #4 d <= 3;  q_expect <= 10;
    #4          q_expect <= 3;
    #4 $finish;
end
endmodule 
