`timescale 100ns/10ns

module test_flash_module;

/* instantiate variables */
reg clk;
parameter ADDR_WIDTH = 16;
parameter DATA_WIDTH = 8;
reg     [ADDR_WIDTH - 1:0] address;
reg     [DATA_WIDTH - 1:0] data;
reg     nEnable, nRead, nWrite, nReset;
wire    decoderEnable;
reg     decoderEnable_ex;
inout wire    [DATA_WIDTH-1:0] d_in_out;
reg     [DATA_WIDTH-1:0] d_out;
wire    [DATA_WIDTH-1:0] d_input;
reg     [DATA_WIDTH-1:0] d_input_ex;

assign d_in_out = (nEnable == 0 && nWrite == 0 && nRead == 1 && nReset == 1)? data :{DATA_WIDTH{1'bz}};
// assign d_input <= (nEnable <=<= 0 && nWrite <=<= 1 && nRead <=<= 0 && nReset <=<= 1)? data :{DATA_WIDTH{1'bz}};
assign d_input = d_in_out;

wire [3:0] EWRS ;    // Enable Write Read Reset
flash_module fm1(
    .address(address),
    .data_in_out(d_in_out),
    // .data(data),
    .nEnable(nEnable),
    .nRead(nRead),
    .nWrite(nWrite),
    .nReset(nReset)
);

/* Make a regular pulsing clock. */
parameter CLOCK_PERIOD = 4;
// always #2 clk <= !clk;

initial begin
    $dumpfile("flash_module.vcd"); // filename of wavefile
    $dumpvars(0,fm1);    // dump instance m1
end
/* setup console monitor format */
// initial begin
//     $display("\ttime, nEnable, nRead, nWrite, nReset, decoderEnable, modeCode, modeCode_ex");
// end

/* setup test condition */
always @ (d_input_ex)
begin
    //$display("\n\t-----------");
    $display("\n\t----------------------");
    if(d_input_ex != d_input)
    begin
        $display("\tREAD ERROR");
    end
    else
    begin
        $display("\tREAD OK");
    end
    $display("\t----------------------");
    $display("\tEXPECTED: 0x%h @0x%h", d_input_ex, address);
    $display("\tRECEIVED: 0x%h @0x%h", d_input, address);
end

parameter CODE_READ             = 8'h00;
parameter CODE_WRITE            = 8'h20;
parameter CODE_FASTWRITE_SET    = 8'hB0;
parameter CODE_FASTWRITE_CONT   = 8'hC0;
parameter CODE_FASTWRITE_RST1   = 8'hD0;
parameter CODE_FASTWRITE_RST2   = 8'hE0;
parameter CODE_RESET            = 8'hF0;

parameter PREAM_ADDR5           = 16'h5555;
parameter PREAM_ADDRA           = 16'hAAAA;
parameter PREAM_DATA5           = 8'h55;
parameter PREAM_DATAA           = 8'hAA;
/* initial conditions */
initial begin
    clk                 <= 0;
    address             <= 0;
    nEnable             <= 1;
    nRead               <= 1;
    nWrite              <= 1;
    nReset              <= 1;
end

/* runtime conditions */
initial
begin
    // test GOOD write command
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= PREAM_DATAA;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDRA;
    data    <= PREAM_DATA5;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= CODE_WRITE;       // write command byte
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h4430;
    data    <= 8'hBC;    // <- data comes here
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;
    // end write command

    // begin read
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= PREAM_DATAA;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDRA;
    data    <= PREAM_DATA5;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= CODE_READ;       // write command byte
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 1; nRead <= 0; nReset <= 1;
    address <= 16'h4430;
    #CLOCK_PERIOD
    // $display("READING... %h", d_input);
    d_input_ex <= 8'hBC;
    // data    <= 8'hBC;    EXPECTED DATA
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;


    // test GOOD write command
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= PREAM_DATAA;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDRA;
    data    <= PREAM_DATA5;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= CODE_WRITE;       // write command byte
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h4440;
    data    <= 8'h79;    // <- data comes here
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    // begin read
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= PREAM_DATAA;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDRA;
    data    <= PREAM_DATA5;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= CODE_READ;       // write command byte
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 1; nRead <= 0; nReset <= 1;
    address <= 16'h4440;
    #CLOCK_PERIOD
    // $display("READING... %h", d_input);
    d_input_ex <= 8'h79;
    // data    <= 8'h79;    EXPECTED DATA
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;



    // test GOOD fast-write command
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= PREAM_DATAA;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDRA;
    data    <= PREAM_DATA5;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= CODE_FASTWRITE_SET;       // write command byte
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;
    // in fast write mode now

    // fast write continue 
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    // address <= 
    data <= CODE_FASTWRITE_CONT;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h4460;
    data <= 8'hF0;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    // fast write continue 
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    // address <= 
    data <= CODE_FASTWRITE_CONT;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h4463;
    data <= 8'hF1;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    // fast write continue 
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    // address <= 
    data <= CODE_FASTWRITE_CONT;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h4465;
    data <= 8'hF2;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    // fast reset 
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;  // not needed for fastwrite reset
    data <= CODE_FASTWRITE_RST1;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDRA;  // not needed for fastwrite reset
    data <= CODE_FASTWRITE_RST2;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;


    // begin read
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= PREAM_DATAA;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDRA;
    data    <= PREAM_DATA5;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= CODE_READ;       // write command byte
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 1; nRead <= 0; nReset <= 1;
    address <= 16'h4460;
    #CLOCK_PERIOD
    // $display("READING... %h", d_input);
    d_input_ex <= 8'hF0;
    // data    <= 8'hF0;    EXPECTED DATA
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;


    // begin read
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= PREAM_DATAA;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDRA;
    data    <= PREAM_DATA5;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= CODE_READ;       // write command byte
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 1; nRead <= 0; nReset <= 1;
    address <= 16'h4463;
    #CLOCK_PERIOD
    // $display("READING... %h", d_input);
    d_input_ex <= 8'hF1;
    // data    <= 8'hF1;    EXPECTED DATA
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    // test GOOD write command
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= PREAM_DATAA;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDRA;
    data    <= PREAM_DATA5;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= CODE_WRITE;       // write command byte
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= 16'h4490;
    data    <= 8'h69;    // <- data comes here
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    // begin read
    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= PREAM_DATAA;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDRA;
    data    <= PREAM_DATA5;
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 0; nRead <= 1; nReset <= 1;
    address <= PREAM_ADDR5;
    data    <= CODE_READ;       // write command byte
    #CLOCK_PERIOD
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;

    #CLOCK_PERIOD
    nEnable <= 0; nWrite <= 1; nRead <= 0; nReset <= 1;
    address <= 16'h4490;
    #CLOCK_PERIOD
    // $display("READING... %h", d_input);
    d_input_ex <= 8'h69;    // EXPECTED DATA
    // data    <= 8'h69;    EXPECTED DATA
    nEnable <= 1; nWrite <= 1; nRead <= 1; nReset <= 1;
end
endmodule
