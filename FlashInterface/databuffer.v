module databuffer(d, q, clk);
parameter WIDTH = 8;

output reg [WIDTH-1: 0] q;
input wire [WIDTH-1: 0] d;
input wire clk;

always@(posedge clk)
begin
    q <= d;
end
endmodule
