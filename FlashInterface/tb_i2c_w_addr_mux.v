`timescale 100ns/10ns

module test_i2c_addr_mux;

/* instantiate variables */
parameter ADDR_WIDTH = 16;
parameter DATA_WIDTH = 8;

reg scl;
reg selAddr, selAAAA, sel5555;

reg    [ADDR_WIDTH-1:0] addrIn;
wire   [ADDR_WIDTH-1:0] addrOut;
reg    [ADDR_WIDTH-1:0] addrOut_ex;


i2c_w_addr_mux i2cAddrMux(
    .addrOut(addrOut),
    .addrIn(addrIn),
    .selAddr(selAddr),
    .selAAAA(selAAAA),
    .sel5555(sel5555),
    .scl(scl)
    );

/* Make a regular pulsing clock. */
parameter CLOCK_PERIOD = 10;
// always #(CLOCK_PERIOD/2) scl <= !scl;

initial begin
    addrIn = 16'hzzzz;
    addrOut_ex = 16'hzzzz;
end

initial begin
    $dumpfile("i2cAddrMux.vcd"); // filename of wavefile
    $dumpvars(0,i2cAddrMux);    // dump instance m1
end


/* setup test condition */
always @ (addrOut)
begin
    //$display("\n\t-----------");
    $display("\n\t----------------------");
    if(addrOut !== addrOut_ex)
    begin
        $display("\tMUX OUTPUT ERROR");
    end
    else
    begin
        $display("\tMUX OUTPUT OK");
    end
    $display("\t----------------------");
    $display("\tEXPECTED: 0x%h", addrOut_ex);
    $display("\tRECEIVED: 0x%h", addrOut);
end

parameter CODE_READ             = 8'h00;
parameter CODE_WRITE            = 8'h20;
parameter CODE_FASTWRITE_SET    = 8'hB0;
parameter CODE_FASTWRITE_CONT   = 8'hC0;
parameter CODE_FASTWRITE_RST1   = 8'hD0;
parameter CODE_FASTWRITE_RST2   = 8'hE0;
parameter CODE_RESET            = 8'hF0;

parameter PREAM_ADDR5           = 16'h5555;
parameter PREAM_ADDRA           = 16'hAAAA;
parameter PREAM_DATA5           = 8'h55;
parameter PREAM_DATAA           = 8'hAA;
/* initial conditions */
initial begin

end

/* runtime conditions */
initial
begin
    addrIn  <= 16'h22; 
    selAddr <= 0; selAAAA <= 0; sel5555 <= 0;
    addrOut_ex <= 16'hzzzz;

    // check for ADDR
    #CLOCK_PERIOD 
    addrIn  <= 8'h49; 
    selAddr <= 1; selAAAA <= 0; sel5555 <= 0; 
    addrOut_ex <= 16'h0049;

    // check for PREAM AAAA
    #CLOCK_PERIOD 
    addrIn  <= 16'h49; 
    selAddr <= 0; selAAAA <= 1; sel5555 <= 0; 
    addrOut_ex <= 16'haaaa;

    // check for PREAM 55555
    #CLOCK_PERIOD 
    #CLOCK_PERIOD 
    addrIn  <= 16'h49; 
    selAddr <= 0; selAAAA <= 0; sel5555 <= 1;
    addrOut_ex <= 16'h5555;

    // check for ZZZZ
    #CLOCK_PERIOD 
    addrIn  <= 16'h49; 
    selAddr <= 0; selAAAA <= 0; sel5555 <= 1;
    addrOut_ex <= 16'hZZZZ;

    #20
    $finish;
end
endmodule