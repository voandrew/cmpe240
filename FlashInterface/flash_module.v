module flash_module(data_in_out, address, nEnable, nRead, nWrite, nReset);
parameter ADDR_WIDTH = 16;
parameter DATA_WIDTH = 8;
input  wire[ADDR_WIDTH - 1:0] address;
input wire     nEnable, nRead, nWrite, nReset;
wire    decoderEnable;

wire [3:0] EWRS ;    // Mem Enable Write Read Reset (direct to banks)

// bidirectional
inout [DATA_WIDTH-1:0] data_in_out;
reg [DATA_WIDTH-1:0] data_out;
wire [DATA_WIDTH-1:0] data_in;


assign data_in_out = (nEnable == 0 && nWrite == 1 && nRead == 0 && nReset == 1) ?  data_in_out : {DATA_WIDTH{1'bz}};
assign data_in = data_in_out;


flash_controller fc1(
    .address(address),
    .data(data_in),
    .nEnable(nEnable),
    .nRead(nRead),
    .nWrite(nWrite),
    .nReset(nReset),
    .decoderEnable(decoderEnable),
    .nMemEnable(EWRS[3]),
    .nMemWrite(EWRS[2]),
    .nMemRead(EWRS[1]),
    .nMemReset(EWRS[0])
);



wire [15:0] bankselect;
wire [3:0] blockselect;
wire [7:0] rowselect;

addr_decoder addr_d_1(
    .address(address),
    .decoderEnable(decoderEnable),
    .bankselect(bankselect),
    .blockselect(blockselect),
    .rowselect(rowselect)
    );

generate
    genvar i;
    for(i = 0; i < 16; i = i + 1)
    begin
        mem_bank bank(
            .data_in_out(data_in_out),
            .block_no(blockselect),
            .addr_block(rowselect),
            .enable(bankselect[i]),
            .write_en(EWRS[2]),
            .read_en(EWRS[1]),
            .reset(EWRS[0])
            );
    end
endgenerate

endmodule
