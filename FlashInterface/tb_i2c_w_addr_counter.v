`timescale 100ns/10ns

module test_i2c_addr_mux;

/* instantiate variables */
parameter ADDR_WIDTH = 16;
parameter DATA_WIDTH = 8;

reg scl;

reg    [ADDR_WIDTH/2-1:0] shiftIn;
wire   [ADDR_WIDTH-1:0] addrOut;
reg    [ADDR_WIDTH-1:0] addrOut_ex;
reg loadAddrLSB, loadAddrMSB, incrAddr;


i2c_w_addr_counter i2cAddrCounter(
    .addrOut    (addrOut),
    .shiftIn    (shiftIn),
    .loadAddrLSB(loadAddrLSB),
    .loadAddrMSB(loadAddrMSB),
    .incrAddr   (incrAddr),
    .scl        (scl)
    );

/* Make a regular pulsing clock. */
parameter CLOCK_PERIOD = 10;
// always #(CLOCK_PERIOD/2) scl <= !scl;

initial begin
    shiftIn = 8'hzz;
    addrOut_ex = 16'hzzzz;
end

initial begin
    $dumpfile("i2cAddrCounter.vcd"); // filename of wavefile
    $dumpvars(0,i2cAddrCounter);    // dump instance m1
end


/* setup test condition */
always @ (addrOut)
begin
    //$display("\n\t-----------");
    $display("\n\t----------------------");
    if(addrOut !== addrOut_ex)
    begin
        $display("\tMUX OUTPUT ERROR");
    end
    else
    begin
        $display("\tMUX OUTPUT OK");
    end
    $display("\t----------------------");
    $display("\tEXPECTED: 0x%h", addrOut_ex);
    $display("\tRECEIVED: 0x%h", addrOut);
end

parameter CODE_READ             = 8'h00;
parameter CODE_WRITE            = 8'h20;
parameter CODE_FASTWRITE_SET    = 8'hB0;
parameter CODE_FASTWRITE_CONT   = 8'hC0;
parameter CODE_FASTWRITE_RST1   = 8'hD0;
parameter CODE_FASTWRITE_RST2   = 8'hE0;
parameter CODE_RESET            = 8'hF0;

parameter PREAM_ADDR5           = 16'h5555;
parameter PREAM_ADDRA           = 16'hAAAA;
parameter PREAM_DATA5           = 8'h55;
parameter PREAM_DATAA           = 8'hAA;
/* initial conditions */
initial begin

end

/* runtime conditions */
initial
begin
    scl <= 1;
    shiftIn  <= 8'h22; 
    loadAddrLSB <= 0; loadAddrMSB <= 0; incrAddr <= 0;
    addrOut_ex <= 16'h0000;
    #CLOCK_PERIOD
    scl <= 0;

    #CLOCK_PERIOD
    scl <= 1;
    shiftIn  <= 8'h22; 
    loadAddrLSB <= 1; loadAddrMSB <= 0; incrAddr <= 0;
    addrOut_ex <= 16'h0022;
    #CLOCK_PERIOD
    scl <= 0;

    #CLOCK_PERIOD
    scl <= 1;
    shiftIn  <= 16'h33; 
    loadAddrLSB <= 0; loadAddrMSB <= 1; incrAddr <= 0;
    addrOut_ex <= 16'h3322;
    #CLOCK_PERIOD
    scl <= 0;

    #CLOCK_PERIOD
    scl <= 1;
    shiftIn  <= 16'h33; 
    loadAddrLSB <= 0; loadAddrMSB <= 0; incrAddr <= 1;
    addrOut_ex <= 16'h3323;
    #CLOCK_PERIOD
    scl <= 0;

    #CLOCK_PERIOD
    scl <= 1;
    shiftIn  <= 16'h33; 
    loadAddrLSB <= 0; loadAddrMSB <= 0; incrAddr <= 1;
    addrOut_ex <= 16'h3324;
    #CLOCK_PERIOD
    scl <= 0;

    #CLOCK_PERIOD
    scl <= 1;
    addrOut_ex <= 16'h3325;
    #CLOCK_PERIOD
    scl <= 0;

    #CLOCK_PERIOD
    scl <= 1;
    addrOut_ex <= 16'h3326;
    #CLOCK_PERIOD
    scl <= 0;

    #CLOCK_PERIOD
    scl <= 1;
    shiftIn  <= 16'h88; 
    loadAddrLSB <= 0; loadAddrMSB <= 1; incrAddr <= 0;
    addrOut_ex <= 16'h8826;
    #CLOCK_PERIOD
    scl <= 0;
    #20
    $finish;
end
endmodule