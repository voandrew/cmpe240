`timescale 100ns/10ns

module test_i2c_w_controller;

/* instantiate variables */
parameter ADDR_WIDTH = 16;
parameter DATA_WIDTH = 8;

// i2c signals
reg scl;
reg sda;

// outputs
wire i2cStart, i2cStop;
wire   [ADDR_WIDTH-1:0] addrOut;
wire   [ADDR_WIDTH-1:0] addrRegOut;
reg    [ADDR_WIDTH-1:0] addrOut_ex;
wire   [DATA_WIDTH-1:0] shiftRegOut;
inout  [DATA_WIDTH-1:0] dataIo; // only view output 

// control signals
wire shiftEnable;
wire loadAddrLSB, loadAddrMSB, incrAddr;
wire selAddr, sel5555, selAAAA;
wire selData, selAA, sel55, selB0, selC0, selD0, selE0, enDataOut;
wire nEn, nWe;

initial begin
    scl <= 1;
    sda <= 1;
    #2
    sda <= 0;
    // nEn <= 1;
    // nWe <= 1;

    // selAddr <= 0; sel5555 <= 0; selAAAA <= 0;
    // selData <= 0; selAA <= 0; sel55 <= 0; selB0 <= 0; selC0 <= 0; selD0 <= 0; selE0 <= 0; enDataOut <= 0;
    end
i2c_w_controller i2cWriteController(
    .shiftEnable(shiftEnable),
    .loadAddrLSB(loadAddrLSB),
    .loadAddrMSB(loadAddrMSB),
    .incrAddr   (incrAddr),
    .selAddr    (selAddr),
    .sel5555    (sel5555),
    .selAAAA    (selAAAA),
    .selData    (selData),
    .selAA      (selAA),
    .sel55      (sel55),
    .selB0      (selB0),
    .selC0      (selC0),
    .selD0      (selD0),
    .selE0      (selE0),
    .enDataOut  (enDataOut),
    .nEn        (nEn),
    .nWe        (nWe),
    // .enSdaOut   (enSdaOut),
    // .loadDevId  (loadDevId),
    // .selAux     (selAux),
    .start      (i2cStart),
    .stop       (i2cStop),
    .scl        (scl),
    .writeBit   (shiftRegOut[7])
    );

i2c_start_stop_detector i2cDetector(
    .scl  (scl),
    .sda  (sda),
    .start(i2cStart),
    .stop (i2cStop)
    );

shift_reg shiftReg1(
    .shiftRegOut(shiftRegOut),
    .shiftIn    (shiftEnable),
    .SDAdataIn  (sda),
    .SCL        (scl),
    .loadShift  (1'b0),
    .shiftRegIn (8'h00),
    .shiftOut   (1'b0)
    // .SDAdataOut (1'bz),
    // .shiftOut   (1'b0)
    );

i2c_w_addr_counter i2cAddrCounter(
    .addrOut    (addrRegOut),
    .shiftIn    (shiftRegOut),
    .loadAddrLSB(loadAddrLSB),
    .loadAddrMSB(loadAddrMSB),
    .incrAddr   (incrAddr),
    .scl        (scl)
    );

i2c_w_data_mux i2cWriteDataMux(
    .dataIo   (dataIo),
    .dataInput(shiftRegOut),
    .selData  (selData),
    .selAA    (selAA),
    .sel55    (sel55),
    .selB0    (selB0),
    .selC0    (selC0),
    .selD0    (selD0),
    .selE0    (selE0),
    .enDataOut(enDataOut),
    .scl      (scl)
    );

i2c_w_addr_mux i2cAddrMux(
    .addrOut(addrOut),
    .selAddr(selAddr),
    .sel5555(sel5555),
    .selAAAA(selAAAA)
    );
/* Make a regular pulsing clock. */
reg killclock = 0;
parameter CLOCK_PERIOD = 10;
always begin
    if(killclock === 0)
    begin
        #(CLOCK_PERIOD/2) scl <= !scl;
    end
end

reg [11:0] count = 0;
always @(negedge scl)
begin 
    count <= count + 1;
    if(count == 47)
    begin
        $display("KILL");
        killclock <= 0;
        scl <= 1;
        sda <= 0;
        #2
        sda <= 1;
    end

end

initial begin
    addrOut_ex = 16'hzzzz;
end

initial begin
    $dumpfile("i2cWriteController.vcd"); // filename of wavefile
    $dumpvars(0,test_i2c_w_controller);    // dump instance m1
end


/* setup test condition */
// always @ (addrOut)
// begin
//     //$display("\n\t-----------");
//     $display("\n\t----------------------");
//     if(addrOut !== addrOut_ex)
//     begin
//         $display("\tMUX OUTPUT ERROR");
//     end
//     else
//     begin
//         $display("\tMUX OUTPUT OK");
//     end
//     $display("\t----------------------");
//     $display("\tEXPECTED: 0x%h", addrOut_ex);
//     $display("\tRECEIVED: 0x%h", addrOut);
// end

parameter CODE_READ             = 8'h00;
parameter CODE_WRITE            = 8'h20;
parameter CODE_FASTWRITE_SET    = 8'hB0;
parameter CODE_FASTWRITE_CONT   = 8'hC0;
parameter CODE_FASTWRITE_RST1   = 8'hD0;
parameter CODE_FASTWRITE_RST2   = 8'hE0;
parameter CODE_RESET            = 8'hF0;

parameter PREAM_ADDR5           = 16'h5555;
parameter PREAM_ADDRA           = 16'hAAAA;
parameter PREAM_DATA5           = 8'h55;
parameter PREAM_DATAA           = 8'hAA;
/* initial conditions */
initial begin

end

/* runtime conditions */
initial
begin
    #1000
    $finish;
end
endmodule