module i2c_w_addr_counter(addrOut, shiftIn, loadAddrLSB, loadAddrMSB, incrAddr, scl);
    parameter ADDR_WIDTH = 16;
    output reg [ADDR_WIDTH - 1: 0] addrOut;
    input wire [ADDR_WIDTH/2 - 1: 0] shiftIn;
    input wire loadAddrLSB, loadAddrMSB, incrAddr, scl;

    wire nSCL;
    assign nSCL = !scl;

initial begin
    addrOut <= 0;
end

    always @(posedge nSCL)
    begin
        if(loadAddrLSB == 1 && loadAddrMSB === 0 && incrAddr === 0)
        begin
            addrOut[7:0] <= shiftIn;
        end
        else if(loadAddrLSB == 0 && loadAddrMSB === 1 && incrAddr === 0)
        begin
            addrOut[15:8] <= shiftIn;
        end
        else if(loadAddrLSB == 0 && loadAddrMSB === 0 && incrAddr === 1)
        begin
            addrOut <= addrOut + 1;
        end
        else
        begin
            addrOut <= addrOut;
        end

    end
endmodule // i2c_w_addr_counter