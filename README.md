# README #

Repository for CMPE240 @ SJSU Spring 2016

### Examples ###

Use the "skel" or "example" directory as a base for Verilog projects.

```git clone https://voandrew@bitbucket.org/voandrew/cmpe240.git```

The included Makefile requires the following tools:

* iverilog ```brew install icarus-verilog```
* vvp (comes when installing ```icarus-verilog```)
* [Scansion](http://www.logicpoet.com/scansion/)

Make commands:

```
make all        // compiles and runs simulator
make compile    // compiles to a .vvp file
make simulate   // runs vvp on the compiled portion
make view       // opens Scansion with any .vcd files
make clean      // cleans up all files generated 
```
