module controller(incA, incB, weB, weA, systemReset, reset, clk);
output reg  incA, incB, weA, weB, systemReset;
input  wire reset, clk;

// initialize a counter module
wire    [4:0] counterValue;
reg     counterReset;

counter c1(.out(counterValue), 
            .reset(counterReset), 
            .clk(clk), 
            .enable(1'd1));

initial begin
    counterReset = 1;
    // counterReset = 0;
end

// positive reset
always@(posedge clk or reset)
begin
    if(reset || counterValue >= 18)
    begin
        counterReset <= 1;
        systemReset  <= 1;
        weA  <= 0;
        weB  <= 0;
        incA <= 0;
        incB <= 0;
    end
    else
    begin
        counterReset = 0;
        systemReset  = 0;
        if((counterValue + 1) < 9)  weA  <= 1;
        else                        weA  <= 0;

        if((counterValue + 1) < 17) incA <= 1;
        else                        incA <= 0;

        if(counterValue >= 10 && counterValue < 18)
        begin
            if((counterValue + 1) % 2 == 0)   
            begin
                incB <= 1;
                weB  <= 0;
            end
            else                              
            begin
                incB <= 0;
                weB  <= 1;
            end
        end
        else 
        begin 
            incB <= 0;
            weB  <= 0;
        end
    end
end
endmodule
