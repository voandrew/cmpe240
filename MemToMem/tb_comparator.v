`timescale 100ns/10ns

module test_comparator;

/* instantiate variables */
wire out;
reg [7:0] d1,d2;

/* initial conditions */
initial begin
    d1 = 0;
    d2 = 0;
end

/* Make a regular pulsing clock. */
// always #2 clk = !clk;

initial begin
    $dumpfile("comparator.vcd"); // filename of wavefile
    $dumpvars(0,comp1);    // dump instance c1
end

/* instantiate module */
comparator comp1(.out(out),
    .in1(d1), .in2(d2));

/* setup console monitor format */
/*
initial begin
    $monitor("At time %t, reset = %b, clk = %b, out = %b",
    $time, reset, clk, out);
end
*/


/* runtime conditions */
initial
begin
    #5   d1 <= 1; d2 <= 3;
    #10  d1 <= 6; d2 <= 3;
    #20  d1 <= 2; d2 <= 4;
    #50  d1 <= 7; d2 <= 7;
    #50  d1 <= 7; d2 <= 8;
    #50  d1 <= 9; d2 <= 8;
    #200 $finish;   // run for 200 time units
end
endmodule // test
