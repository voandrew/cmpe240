`timescale 100ns/10ns

module test_operation_sel;

/* instantiate variables */
reg [7:0] d1,d2;
wire[7:0] out;
reg sel;

/* initial conditions */


/* Make a regular pulsing clock. */
// always #2 clk = !clk;

initial begin
    $dumpfile("op_sel.vcd"); // filename of wavefile
    $dumpvars(0,op_sel1);    // dump instance c1
end

/* instantiate module */
operation_sel op_sel1(.out(out),
    .in1(d1), 
	.in2(d2),
	.sel(sel));

/* setup console monitor format */
/*
initial begin
    $monitor("At time %t, reset = %b, clk = %b, out = %b",
    $time, reset, clk, out);
end
*/


/* runtime conditions */
initial begin
    #0  d1 = 0; d2 = 1; sel=0;
    #10 d1 = 0; d2 = 1; sel=1;
    #10 d1 = 10; d2 = 5; sel=0;
    #10 d1 = 10; d2 = 5; sel=1;
    #10 d1 = 7; d2 = 2; sel=0;
    #10 d1 = 7; d2 = 2; sel=1;
    #10 d1 = 3; d2 = 6; sel=0;
    #10 d1 = 3; d2 = 6; sel=1;
	#100 $finish;	
end
endmodule // test
