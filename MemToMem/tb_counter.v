`timescale 100ns/10ns

module test_counter;

/* instantiate variables */
reg clk = 1;
reg reset, enable;
wire [4:0]  out;

/* initial conditions */
initial begin
    clk = 0;
    reset = 1;
    enable = 1;
end

/* Make a regular pulsing clock. */
always #2 clk = !clk;

initial begin
    $dumpfile("counter.vcd"); // filename of wavefile
    $dumpvars(0,c1);    // dump instance c1
end

/* instantiate module */
counter c1(.out(out),
            .clk(clk),
            .enable(enable),
            .reset(reset));

/* setup console monitor format */
/*
initial begin
    $monitor("At time %t, reset = %b, clk = %b, out = %b",
    $time, reset, clk, out);
end
*/


/* runtime conditions */
initial
begin
    #5  reset = 0;
    #50 reset = 1;
    #55 reset = 0;
    #58 enable = 0;
    #75 enable = 1;
    #200 $finish;   // run for 200 time units
end
endmodule // test
