`timescale 100ns/10ns

module test_dflipflop;

/* instantiate variables */
reg     clk = 1;
reg     d;
wire    q;

/* initial conditions */
initial begin
    clk = 0;
    d = 0;
end

/* Make a regular pulsing clock. */
always #2 clk = !clk;

initial begin
    $dumpfile("dflipflop.vcd"); // filename of wavefile
    $dumpvars(0,df1);    // dump instance m1
end

/* instantiate module */
dflipflop df1(.d(d),
            .q(q),
            .clk(clk));

/* setup console monitor format */
initial begin
/*    $monitor("At time %t, reset = %b, clk = %b, out = %b",
    $time, reset, clk, out); 
*/
end


/* runtime conditions */
initial
begin
    #11 d = 1;
    #12 d = 0;
    #200 $finish;    // run for 200 time units
end
endmodule 
