// if in1 > in2 : out = 1
module comparator(out, in1, in2);
parameter WIDTH = 8;

output reg              out;
input  wire[WIDTH-1:0]  in1,in2;

always@(in1, in2)
begin
    if(in1 < in2)
    begin
        out <= 1;
    end
    else
    begin
        out <= 0;
    end
end
endmodule
