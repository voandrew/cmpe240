module dflipflop(d, q, clk);
output reg q;
input wire d, clk;

always@(posedge clk)
begin
    q <= d;
end
endmodule
