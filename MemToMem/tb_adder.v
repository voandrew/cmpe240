// `define WIDTH 8;
`timescale 100ns/10ns


module test_adder;


reg [7:0] Op1, Op2;
wire [7:0] Sum;

// initial conditions
initial begin

end

initial begin
    $dumpfile("adder.vcd"); // filename of wavefile
    $dumpvars(0,add1);    // dump instance m1
end

/* instantiate module */
adder add1(.sum(Sum),
            .op1(Op1),
            .op2(Op2));


/* runtime conditions */
initial
begin

#10 Op1 <= 8'd0; Op2 <= 8'd9;
#10 Op1 <= 8'd1; Op2 <= 8'd8;
#10 Op1 <= 8'd2; Op2 <= 8'd7;
#10 Op1 <= 8'd3; Op2 <= 8'd6;
#10 Op1 <= 8'd4; Op2 <= 8'd5;
#10 Op1 <= 8'd5; Op2 <= 8'd4;
#10 Op1 <= 8'd6; Op2 <= 8'd3;
#10 Op1 <= 8'd7; Op2 <= 8'd2;
#10 Op1 <= 8'd8; Op2 <= 8'd1;
#10 Op1 <= 8'd9; Op2 <= 8'd0;
#10 Op1 <= 8'd0; Op2 <= 8'd1;
#10 Op1 <= 8'd1; Op2 <= 8'd2;
#10 Op1 <= 8'd2; Op2 <= 8'd3;
#10 Op1 <= 8'd3; Op2 <= 8'd4;
#10 Op1 <= 8'd4; Op2 <= 8'd5;
#10 Op1 <= 8'd5; Op2 <= 8'd6;
#10 Op1 <= 8'd6; Op2 <= 8'd7;
#10 Op1 <= 8'd7; Op2 <= 8'd8;
#10 Op1 <= 8'd8; Op2 <= 8'd9;
#10 Op1 <= 8'd9; Op2 <= 8'd0;
#10 $finish;
end



endmodule