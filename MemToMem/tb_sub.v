// `define WIDTH 8;
`timescale 100ns/10ns


module test_sub;


reg [7:0] Op1, Op2;
wire [7:0] Sum;

// initial conditions
initial begin

end

initial begin
    $dumpfile("sub.vcd"); // filename of wavefile
    $dumpvars(0,sub1);    // dump instance m1
end

/* instantiate module */
sub sub1(.sum(Sum),
            .op1(Op1),
            .op2(Op2));


/* runtime conditions */
initial
begin

#10 Op1 <= 8'd0; Op2 <= 8'd9; // 0 - 9 = -9
#10 Op1 <= 8'd1; Op2 <= 8'd8; // 1 - 8 = -7
#10 Op1 <= 8'd2; Op2 <= 8'd7; // 2 - 7 = -5
#10 Op1 <= 8'd3; Op2 <= 8'd6; // 3 - 6 = -3
#10 Op1 <= 8'd4; Op2 <= 8'd5; // 4 - 5 = -1
#10 Op1 <= 8'd5; Op2 <= 8'd4; // 5 - 4 = 1
#10 Op1 <= 8'd6; Op2 <= 8'd3; // 6 - 3 = 3
#10 Op1 <= 8'd7; Op2 <= 8'd2; // 7 - 2 = 5
#10 Op1 <= 8'd8; Op2 <= 8'd1; // 8 - 1 = 7
#10 Op1 <= 8'd9; Op2 <= 8'd0; // 9 - 0 = 9
#10 Op1 <= 8'd0; Op2 <= 8'd1; // 0 - 1 = -1
#10 Op1 <= 8'd1; Op2 <= 8'd2; // 1 - 2 = -1
#10 Op1 <= 8'd2; Op2 <= 8'd3; // 2 - 3 = -1
#10 Op1 <= 8'd3; Op2 <= 8'd4; // 3 - 4 = -1
#10 Op1 <= 8'd4; Op2 <= 8'd5; // 4 - 5 = -1
#10 Op1 <= 8'd5; Op2 <= 8'd6; // 5 - 6 = -1
#10 Op1 <= 8'd6; Op2 <= 8'd7; // 6 - 7 = -1
#10 Op1 <= 8'd7; Op2 <= 8'd8; // 7 - 8 = -1
#10 Op1 <= 8'd8; Op2 <= 8'd9; // 8 - 9 = -1
#10 Op1 <= 8'd9; Op2 <= 8'd0; // 9 - 0 = 9
#10 $finish;
end



endmodule