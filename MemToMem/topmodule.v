`timescale 100ns/10ns

module topmodule(memOut, dataIn, reset, clk);

/* instantiate variables */
output wire [7:0]    memOut;
input wire [7:0]    dataIn;
input wire          reset;
input wire          clk;

wire incA;
wire incB;
wire weB; 
wire weA; 
wire sysReset;

wire [7:0] addOut, subOut;
wire [7:0] dataOut1, dataOut2, dataInB;
wire [2:0] addrA;
wire [1:0] addrB;
wire sign;

// assign memOut = dataOutB;

/* instantiate module */
controller ctrl1(.incA(incA), .incB(incB),
                    .weA(weA), .weB(weB),
                    .systemReset(sysReset),
                    .reset(reset), .clk(clk));
counter counterA(.enable(incA),
                    .out(addrA),
                    .reset(sysReset),
                    .clk(clk));
counter counterB(.enable(incB),
                    .out(addrB),
                    .reset(sysReset),
                    .clk(clk));    
memory  memoryA(.data_out(dataOut1),
                    .write_enable(weA),
                    .addr(addrA),
                    .data_in(dataIn),
                    .clk(clk));
memory  memoryB(.data_out(memOut),
                    .write_enable(weB),
                    .addr(addrB),
                    .data_in(dataInB),
                    .clk(clk));
comparator comp1(.out(sign),
                    .in1(dataOut2),
                    .in2(dataOut1));

adder add1(.sum(addOut),
            .op1(dataOut2),
            .op2(dataOut1));

sub sub1(.sum(subOut),
            .op1(dataOut2),
            .op2(dataOut1));

operation_sel opSel(.out(dataInB),
                    .in1(addOut),
                    .in2(subOut),
                    .sel(sign));

dflipflop d7(.d(dataOut1[7]),
                .q(dataOut2[7]),
                .clk(clk));
dflipflop d6(.d(dataOut1[6]),
                .q(dataOut2[6]),
                .clk(clk));
dflipflop d5(.d(dataOut1[5]),
                .q(dataOut2[5]),
                .clk(clk));
dflipflop d4(.d(dataOut1[4]),
                .q(dataOut2[4]),
                .clk(clk));
dflipflop d3(.d(dataOut1[3]),
                .q(dataOut2[3]),
                .clk(clk));
dflipflop d2(.d(dataOut1[2]),
                .q(dataOut2[2]),
                .clk(clk));
dflipflop d1(.d(dataOut1[1]),
                .q(dataOut2[1]),
                .clk(clk));
dflipflop d0(.d(dataOut1[0]),
                .q(dataOut2[0]),
                .clk(clk));
endmodule // test
