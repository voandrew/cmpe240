module memory(data_out,write_enable,addr,data_in,clk);
parameter WIDTH = 8;
parameter HEIGHT = 8;

output reg [WIDTH-1:0]  data_out;
input  wire[WIDTH-1:0]  data_in;
input  wire[HEIGHT-1:0]  addr;
input  wire             write_enable, clk;

reg [WIDTH-1:0] sram [HEIGHT-1:0];

// initialize sram to 0
integer i;
initial begin
    for(i=0; i<HEIGHT; i++)
    begin
        sram[i] <= 0;
    end
end

always@(posedge clk or write_enable)
begin
    if(write_enable)
    begin
        sram[addr] <= data_in;
    end
    // always read data
    data_out <= sram[addr];
end
endmodule
