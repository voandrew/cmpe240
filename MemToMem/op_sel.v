// if sel == 1 : in1 else in2
module operation_sel(out, in1, in2, sel);
parameter WIDTH = 8;

output reg[WIDTH-1:0]   out;
input  wire[WIDTH-1:0]  in1,in2;
input sel;

always@(in1 or in2 or sel)
begin
    if(sel == 1)
	    begin
	        out <= in1;
	    end
    else
	    begin
	        out <= in2;
	    end
end
endmodule
