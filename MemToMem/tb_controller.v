`timescale 100ns/10ns

module test_counter;

/* instantiate variables */
reg clk = 1;
reg reset;
wire incA, incB, weB, weA, sysReset;

/* initial conditions */
initial begin
    clk = 0;
    reset = 1;
end

/* Make a regular pulsing clock. */
always #2 clk = !clk;

initial begin
    $dumpfile("controller.vcd"); // filename of wavefile
    $dumpvars(0,ctrl1);    // dump instance c1
end

/* instantiate module */
controller ctrl1(.incA(incA), .incB(incB),
                    .weA(weA), .weB(weB),
                    .systemReset(sysReset),
                    .reset(reset), .clk(clk));
/* setup console monitor format */
/*
initial begin
    $monitor("At time %t, reset = %b, clk = %b, out = %b",
    $time, reset, clk, out);
end
*/


/* runtime conditions */
initial
begin
    #10 reset = 0;
    #200 $finish;   
end
endmodule // test
