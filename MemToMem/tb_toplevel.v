`timescale 100ns/10ns

module test_system;

/* instantiate variables */
reg clk = 1;
reg reset;
wire [7:0] memOut;
reg  [7:0] dataInA;

/* initial conditions */
initial begin
    clk = 0;
    reset = 1;
    dataInA = 8'd2;
end

/* Make a regular pulsing clock. */
always 
begin
    #2 clk = !clk;
end

always@(posedge clk)
begin
    dataInA = $random;
end
initial begin
    $dumpfile("toplevel.vcd"); // filename of wavefile
    $dumpvars(0,test_system);    // dump instance 
end

topmodule m1(.memOut(memOut),
                .dataIn(dataInA),
                .reset(reset),
                .clk(clk));


/* instantiate module */
/* setup console monitor format */
/*
initial begin
    $monitor("At time %t, reset = %b, clk = %b, out = %b",
    $time, reset, clk, out);
end
*/


/* runtime conditions */
initial
begin
    #2 reset = 0;
    #200 $finish;   
end
endmodule // test
