`timescale 100ns/10ns

module test_memory;

/* instantiate variables */
reg             clk = 1;
reg             wren = 0;
reg     [2:0]   addr;   // memory has only 8 addresses
wire    [7:0]   dout;
reg     [7:0]   din;

/* initial conditions */
initial begin
    clk = 0;
    wren = 0;
    addr = 0;
    din = 8'hA2;    // sample input data
end

/* Make a regular pulsing clock. */
always #2 clk = !clk;

always #2 addr = addr + 1;

initial begin
    $dumpfile("memory.vcd"); // filename of wavefile
    $dumpvars(0,m1);    // dump instance m1
end

/* instantiate module */
memory m1(.data_out(dout),
            .write_enable(wren),
            .clk(clk),
            .addr(addr),
            .data_in(din));

/* setup console monitor format */
initial begin
/*    $monitor("At time %t, reset = %b, clk = %b, out = %b",
    $time, reset, clk, out); 
*/
end


/* runtime conditions */
initial
begin
    #10 wren = 1;   // write data
    #15 wren = 0;   // stop writing
    #200 $finish;   // run for 200 time units
end
endmodule 
