// subtraction module
// op1 - op2 = sum

module sub(sum, op1, op2);

	parameter WIDTH = 8;
	
	input wire [WIDTH-1:0] op1, op2;
	
	output wire [WIDTH-1:0] sum;

	assign sum = op1 - op2;

endmodule